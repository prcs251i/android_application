package com.example.jaderedworth.androidmobileapplication.JSONModels;

/**
 * Created by jaderedworth on 16/05/2016.
 */
public class AuthenticationHolder {
    private static CustomerModel instance;
    public static CustomerModel getInstance(){
        return AuthenticationHolder.instance;
    }
    public static void setInstance(CustomerModel data){
        AuthenticationHolder.instance = data;
    }
}
