package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jaderedworth on 24/04/2016.
 */
public class VenueModel implements Serializable{

    private int id;
    private String venueName;
    private String VenueAddressLine1;
    private String VenueAddressLine2;
    private String VenueAddressCity;
    private String VenueAddressCounty;
    private String VenueAddressPostcode;
    private List<TierModel> tierList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVenueName()
    {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueAddressLine1() {
        return VenueAddressLine1;
    }

    public void setVenueAddressLine1(String venueAddressLine1) {
        VenueAddressLine1 = venueAddressLine1;
    }

    public String getVenueAddressLine2() {
        return VenueAddressLine2;
    }

    public void setVenueAddressLine2(String venueAddressLine2) {
        VenueAddressLine2 = venueAddressLine2;
    }

    public String getVenueAddressCity() {
        return VenueAddressCity;
    }

    public void setVenueAddressCity(String venueAddressCity) {
        VenueAddressCity = venueAddressCity;
    }

    public String getVenueAddressCounty() {
        return VenueAddressCounty;
    }

    public void setVenueAddressCounty(String venueAddressCounty) {
        VenueAddressCounty = venueAddressCounty;
    }

    public String getVenueAddressPostcode() {
        return VenueAddressPostcode;
    }

    public void setVenueAddressPostcode(String venueAddressPostcode) {
        VenueAddressPostcode = venueAddressPostcode;
    }

    public List<TierModel> getTierList() {
        return tierList;
    }

    public void setTierList(List<TierModel> tierList) {
        this.tierList = tierList;
    }
}
