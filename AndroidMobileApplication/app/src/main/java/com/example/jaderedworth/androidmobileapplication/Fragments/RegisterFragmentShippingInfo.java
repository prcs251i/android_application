package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.jaderedworth.androidmobileapplication.DataValidator;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R;

public class RegisterFragmentShippingInfo extends Fragment {
    private EditText addressOneInput;
    private EditText addressTwoInput;
    private EditText postcodeInput;
    private EditText cityInput;
    private EditText countyInput;
    private Button proceedButton;
    private Button backButton;
    private String addressOne;
    private String addressTwo;
    private String postcode;
    private String city;
    private String county;

    private Bundle oldUserData;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View rootView = inflater.inflate(R.layout.fragment_register_shipping_info, container, false);

        oldUserData = this.getArguments();
        findInputFields(rootView);

        proceedButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                readUserInput();
                if (isValidInput()) {
                    Bundle userData;
                    userData = prepareDataToPass(oldUserData);
                    RegisterFragmentPaymentInfo targetFragment = new RegisterFragmentPaymentInfo();
                    targetFragment.setArguments(userData);
                    FragmentTransaction t = getFragmentManager().beginTransaction();
                    t.replace(R.id.fragment_login_main, targetFragment);
                    t.addToBackStack(null);
                    t.commit();
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragmentMain targetFragment = new RegisterFragmentMain(getContext());
                FragmentTransaction t = getFragmentManager().beginTransaction();
                t.replace(R.id.fragment_login_main, targetFragment);
                t.addToBackStack(null);
                t.commit();
            }
        });
        return rootView;
    }

    public void findInputFields(View tempView){
        addressOneInput = (EditText) tempView.findViewById(R.id.editTextAddress1);
        addressTwoInput = (EditText) tempView.findViewById(R.id.editTextAddress2);
        postcodeInput = (EditText)tempView.findViewById(R.id.editTextPostcode);
        cityInput = (EditText)tempView.findViewById(R.id.editTextCity);
        countyInput = (EditText)tempView.findViewById(R.id.editTextCounty);
        proceedButton = (Button)tempView.findViewById(R.id.btnCompleteRegistrationStepTwo);
        backButton = (Button)tempView.findViewById(R.id.buttonBackRegistrationStepTwo);
    }

    public void readUserInput(){
        addressOne = addressOneInput.getText().toString();
        addressTwo = addressTwoInput.getText().toString();
        postcode = postcodeInput.getText().toString();
        city = cityInput.getText().toString();
        county = countyInput.getText().toString();
    }

    public Boolean isValidInput(){
        Boolean inputIsValid = false;//
        DataValidator dataValidator = new DataValidator(getContext());

        Boolean validAddressOne = false;
        Boolean validAddressTwo = false;
        Boolean validPostCode = false;
        Boolean validCity = false;
        Boolean validCounty = false;


        dataValidator.setArguments(addressOne, "Address Line One");
        validAddressOne = dataValidator.fieldIsNotEmpty()&&dataValidator.addressIsValid();
        dataValidator.setArguments(postcode, "Post Code");
        validPostCode = dataValidator.fieldIsNotEmpty()&&dataValidator.addressIsValid();//AddrIsVal used so Whitespace is stripped out
        dataValidator.setArguments(city, "City");
        validCity = dataValidator.fieldIsNotEmpty()&&dataValidator.alphabeticStringIsValid();
        dataValidator.setArguments(county, "County");
        validCounty = dataValidator.fieldIsNotEmpty()&&dataValidator.alphabeticStringIsValid();

        //Nullable args
        dataValidator.setArguments(addressTwo, "Address Line Two");
        validAddressTwo = dataValidator.addressIsValid();

        if(validAddressOne&&validPostCode&&validCounty&&validCity){
            inputIsValid =true;
        }
        return inputIsValid;
    }


    public Bundle prepareDataToPass(Bundle previousUserData){
        Bundle newBundle = new Bundle();
        newBundle.putAll(previousUserData);
        newBundle.putString("ADDRESS_LINE_1", addressOne);
        newBundle.putString("ADDRESS_LINE_2", addressTwo);
        newBundle.putString("POSTCODE", postcode);
        newBundle.putString("CITY", city);
        newBundle.putString("COUNTY",county);
        return newBundle;
    }
}
