package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;

public class StandingTicketModel extends TicketModel implements Serializable {
    
    public StandingTicketModel() {
        
    }
    
    public StandingTicketModel(int ref, int bookingRef, VenueModel venue, TierModel tierModel, double price) {
        super(ref, bookingRef, venue, tierModel, price);
    }
}
