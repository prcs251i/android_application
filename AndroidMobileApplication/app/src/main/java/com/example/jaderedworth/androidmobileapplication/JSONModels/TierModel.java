package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;

public class TierModel implements Serializable {
    
    private int tierId;
    private int venueId;
    private String tierName;
    private boolean seated;
    private int capacity;
    private int seatRows;
    private int seatColumns;
    
    public TierModel() {
        
    }
    
    public TierModel(int venueId, String name, boolean isSeated, int totalCapacity, int noOfSeatRows, int noOfSeatColumns) {
        this.venueId = venueId;
        this.tierName = name;
        this.seated = isSeated;
        this.capacity = totalCapacity;
        this.seatRows = noOfSeatRows;
        this.seatColumns = noOfSeatColumns;
    }
    
    public int getTierId() {
        return this.tierId;
    }
    
    public void setTierId(int newTierId) {
        this.tierId = newTierId;
    }
    
    public int getVenueId() {
        return this.venueId;
    }
    
    public void setVenueId(int newVenueId) {
        this.venueId = newVenueId;
    }

    public boolean getIsSeated() {
        return this.seated;
    }
        
    public void setIsSeated(boolean newIsSeated) {
        this.seated = newIsSeated;
    }
    
    public int getCapacity() {
        return this.capacity;
    }
        
    public void setCapacity(int newCapacity) {
        this.capacity = newCapacity;
    }
    
    public int getSeatRows() {
        return this.seatRows;
    }
        
    public void setSeatRows(int newSeatRows) {
        this.seatRows = newSeatRows;
    }
    
    public int getSeatColumns() {
        return this.seatColumns;
    }
        
    public void setSeatColumns(int newSeatColumns) {
        this.seatColumns = newSeatColumns;
    }

    public String getTierName() {
        return this.tierName;
    }

    public void setTierName(String newTierName) {
        this.tierName = newTierName;
    }

}
