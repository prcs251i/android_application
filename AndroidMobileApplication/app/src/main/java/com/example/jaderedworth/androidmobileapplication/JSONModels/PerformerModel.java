package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;

/**
 * Created by jaderedworth on 24/04/2016.
 */
public class PerformerModel implements Serializable{

    private int id;
    private String performerName;
    private String desctription;
    private String actType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPerformerName() {
        return performerName;
    }

    public void setPerformerName(String performerName) {
        this.performerName = performerName;
    }

    public String getDesctription() {
        return desctription;
    }

    public void setDesctription(String desctription) {
        this.desctription = desctription;
    }

    public String getActType() {
        return actType;
    }

    public void setActType(String actType) {
        this.actType = actType;
    }
}
