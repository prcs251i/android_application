package com.example.jaderedworth.androidmobileapplication;

import android.os.AsyncTask;
import android.util.Log;

import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.PerformerModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.PriceModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.ReviewModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TierModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.VenueModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jaderedworth on 06/05/2016.
 */
public class JSONTaskEvent extends AsyncTask<String, String, ArrayList<EventModel>> {

    ArrayList<EventModel> eventModelList;

    public interface AsyncResponse {
        void processFinish(ArrayList<EventModel> output);
    }

    public AsyncResponse delegate = null;

    public JSONTaskEvent(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected ArrayList<EventModel> doInBackground(String... params) {

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            String finalJSON = buffer.toString();
            JSONArray parentArray = new JSONArray(finalJSON);

            eventModelList = new ArrayList();
            for (int i = 0; i < parentArray.length(); i++) {
                JSONObject finalObject = parentArray.getJSONObject(i);
                EventModel eventModel = new EventModel();
                eventModel.setId(finalObject.getInt("ID"));
                JSONObject eventTypeObject = finalObject.getJSONObject("TYPE");
                eventModel.setEventType(eventTypeObject.getString("NAME"));
                eventModel.setTitle(finalObject.getString("TITLE"));
                eventModel.setAge(finalObject.getString("AGE_RATING"));
                eventModel.setDescription(finalObject.getString("DESCRIPTION"));
                eventModel.setImage(finalObject.getString("IMAGE"));
                double avgRating = 0.0;
                int count = 0;
                for(int j = 0; j < finalObject.getJSONArray("REVIEWS").length(); j++){
                    JSONObject reviewsObject = finalObject.getJSONArray("REVIEWS").getJSONObject(j);
                    avgRating +=  reviewsObject.getInt("USER_RATING");
                    count++;
                }

                eventModel.setRating(avgRating / count);
                List<RunModel> runModelList = new ArrayList();
                for (int k = 0; k < finalObject.getJSONArray("RUNS").length(); k++) {
                    RunModel runModel = new RunModel();
                    JSONObject runObject = finalObject.getJSONArray("RUNS").getJSONObject(k);
                    runModel.setId(runObject.getInt("ID"));

                    List<PerformerModel> performerModelList = new ArrayList();
                    for (int l = 0; l < runObject.getJSONArray("PERFORMANCES").length(); l++) {
                        PerformerModel performerModel = new PerformerModel();
                        JSONObject performancesObject = runObject.getJSONArray("PERFORMANCES").getJSONObject(l);
                        JSONObject actsObject = performancesObject.getJSONObject("ACT");
                        JSONObject actTypeObject = actsObject.getJSONObject("TYPE");
                        performerModel.setActType(actTypeObject.getString("NAME"));
                        performerModel.setPerformerName(actsObject.getString("TITLE"));
                        performerModelList.add(performerModel);
                    }
                    HashMap<TierModel, Double> priceList = new HashMap();
                    for (int m = 0; m < runObject.getJSONArray("PRICES").length(); m++) {
                        PriceModel priceModel = new PriceModel();
                        JSONObject priceObject = runObject.getJSONArray("PRICES").getJSONObject(m);
                        priceModel.setId(priceObject.getInt("ID"));
                        priceModel.setPrice(priceObject.getDouble("PRICE1"));
                        priceModel.setTierId(priceObject.getInt("TIER_ID"));
                        priceModel.setRunId(priceObject.getInt("RUN_ID"));
                        TierModel tierPriceModel = new TierModel();
                        JSONObject priceTierObject = priceObject.getJSONObject("TIER");
                        tierPriceModel.setTierId(priceObject.getInt("TIER_ID"));
                        if (priceTierObject.get("SEATED").equals("Y")) {
                            tierPriceModel.setIsSeated(true);
                        } else if (priceTierObject.get("SEATED").equals("N")) {
                            tierPriceModel.setIsSeated(false);
                        }
                        tierPriceModel.setTierName(priceTierObject.getString("TIER_NAME"));
                        priceList.put(tierPriceModel, priceObject.getDouble("PRICE1"));
                    }

                    runModel.setTierPrices(priceList);
                    JSONObject venueObject = runObject.getJSONObject("VENUE");
                    VenueModel venueModel = new VenueModel();
                    venueModel.setVenueName(venueObject.getString("NAME"));
                    venueModel.setVenueAddressLine1(venueObject.getString("ADDRESS_LINE_1"));
                    venueModel.setVenueAddressLine2(venueObject.getString("ADDRESS_LINE_2"));
                    venueModel.setVenueAddressCity(venueObject.getString("CITY"));
                    venueModel.setVenueAddressCounty(venueObject.getString("COUNTY"));
                    venueModel.setVenueAddressPostcode(venueObject.getString("POSTCODE"));

                    List tierModelList = new ArrayList();
                    for (int m = 0; m < venueObject.getJSONArray("TIERS").length(); m++) {
                        TierModel tierModel = new TierModel();
                        JSONObject tierObject = venueObject.getJSONArray("TIERS").getJSONObject(m);

                        tierModel.setTierId(tierObject.getInt("ID"));
                        tierModel.setTierName(tierObject.getString("TIER_NAME"));
                        tierModel.setIsSeated(Boolean.parseBoolean(tierObject.getString("SEATED")));
                        tierModel.setCapacity(tierObject.getInt("CAPACITY"));
                        if (tierObject.get("SEATED").equals("Y")) {
                            tierModel.setIsSeated(true);
                            tierModel.setSeatRows(tierObject.getInt("SEAT_ROWS"));
                            tierModel.setSeatColumns(tierObject.getInt("SEAT_COLUMNS"));
                        } else {
                            tierModel.setIsSeated(false);
                            tierModel.setSeatRows(0);
                            tierModel.setSeatColumns(0);
                        }
                        tierModelList.add(tierModel);
                    }
                    venueModel.setTierList(tierModelList);

                    runModel.setDate(runObject.getString("RUN_DATE"));
                    runModel.setDuration(runObject.getInt("DURATION"));
                    runModel.setPerformers(performerModelList);
                    runModel.setVenue(venueModel);
                    runModelList.add(runModel);
                    eventModel.setRuns(runModelList);
                }
                eventModelList.add(eventModel);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return eventModelList;
    }

    @Override
    protected void onPostExecute(ArrayList<EventModel> result) {
        delegate.processFinish(result);
    }
}