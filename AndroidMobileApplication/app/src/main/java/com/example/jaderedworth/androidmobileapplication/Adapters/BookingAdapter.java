package com.example.jaderedworth.androidmobileapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.Activities.TicketDetailsActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.PerformerModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TicketModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TierModel;
import com.example.jaderedworth.androidmobileapplication.R;
import com.example.jaderedworth.androidmobileapplication.JSONModels.Booking;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jaderedworth on 04/05/2016.
 */
public class BookingAdapter extends ArrayAdapter {

    private List<Booking> bookingModelList;
    private List<EventModel> eventModelList;
    private int resource;
    private Context context;
    LayoutInflater inflater;
    EventModel event;
    private RunModel bookedRun;
    private Bundle persistedData;

    public BookingAdapter(Context context, int resource, List<Booking> objects, List<EventModel> event, Bundle bundle) {
        super(context, resource, objects);
        if(objects!=null) {
            bookingModelList = objects;
        }
        if(event!=null){
            eventModelList = event;
        }
        this.resource = resource;
        this.context = context;
        inflater = (LayoutInflater) getContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.persistedData = bundle;
    }

    @Override
    public int getCount() {
        if(bookingModelList==null) {}
            return bookingModelList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_booking_layout, null);

            viewHolder.bookingRef = (TextView) convertView.findViewById(R.id.textViewBookingRef);
            viewHolder.eventName = (TextView) convertView.findViewById(R.id.textViewBookingEvent);
            viewHolder.venue = (TextView) convertView.findViewById(R.id.textViewBookingVenue);
            viewHolder.datePlaced = (TextView) convertView.findViewById(R.id.textViewBookingDate);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if(bookingModelList!=null) {
            int runID = bookingModelList.get(position).getRunID();
            int size = eventModelList.size();
            for (int i = 0; i < size; i++) {
                if (eventModelList != null) {
                    try {
                        for (RunModel run : eventModelList.get(i).getRuns()) {
                            if (run.getId() == runID) {
                                viewHolder.venue.setText(run.getVenue().getVenueName());
                                viewHolder.eventName.setText(eventModelList.get(i).getTitle());
                                bookedRun = run;
                            }
                        }
                    } catch (NullPointerException ex) {

                    }
                }
            }
            viewHolder.bookingRef.setText(String.valueOf(bookingModelList.get(position).getBookingID()));

            Date date = bookingModelList.get(position).getDatePlaced();
            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
            String dateFormatPlaced = fmt.format(date);
            viewHolder.datePlaced.setText(dateFormatPlaced.toString());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<TicketModel> tickets = bookingModelList.get(position).getTicketList();
                    Intent intent = new Intent(context, TicketDetailsActivity.class);

                    intent.putExtras(persistedData);
                    intent.putExtra("EVENT", eventModelList.get(position));
                    intent.putExtra("RUN", bookedRun);
                    intent.putExtra("TICKETS", (Serializable) tickets);
                    intent.putExtra("EVENT_NAME", viewHolder.eventName.getText());
                    intent.putExtra("VENUE_NAME", viewHolder.venue.getText());
                    intent.putExtra("BOOKING_REF", viewHolder.bookingRef.getText().toString());
                    context.startActivity(intent);
                }
            });
        } else {

        }

        return convertView;
    }

    public class ViewHolder {
        TextView bookingRef;
        TextView venue;
        TextView eventName;
        TextView datePlaced;
    }

}




