package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jaderedworth on 24/04/2016.
 */
public class RunModel implements Serializable {

    private int id;
    private VenueModel venue;
    private String date;
    private int duration;
    private List<PerformerModel> performers;
    private HashMap<TierModel, Double> tierPrices;


    public RunModel(){
        tierPrices = new HashMap();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public VenueModel getVenue() {
        return venue;
    }

    public void setVenue (VenueModel venue) {
        this.venue = venue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<PerformerModel> getPerformers() {
        return performers;
    }

    public void setPerformers(List<PerformerModel> performers) {
        this.performers = performers;
    }


    public HashMap<TierModel, Double> getTierPrices() {
        return tierPrices;
    }

    public void setTierPrices(HashMap<TierModel, Double> tierPrices) {
        this.tierPrices = tierPrices;
    }
}