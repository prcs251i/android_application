package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.example.jaderedworth.androidmobileapplication.Adapters.EventAdapter;
import com.example.jaderedworth.androidmobileapplication.AsyncLoginTask;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONTaskEvent;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class EventsFragment extends Fragment {

    public ListView listViewEvents;
    EventAdapter adapter;
    ArrayList<EventModel> eventList;
    ArrayList<EventModel> filteredList;
    Context c;
    private String email;
    public int userID;
    private Bundle bundle;

    public EventsFragment() {
    }

    public EventsFragment(Context context) {
        c = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_events, container, false);

        bundle = this.getArguments();
        if (bundle != null) {
            eventList = (ArrayList<EventModel>) bundle.getSerializable("EVENT");
            email = bundle.getString("EMAIL");
            userID = bundle.getInt("USER_ID", 0);
        }

        listViewEvents = (ListView) rootView.findViewById(R.id.listViewEvents);
        fillAdapter(eventList);
        doSearch(rootView);
        return rootView;
    }

    private void doSearch(View rootView) {
        final EditText et = (EditText) rootView.findViewById(R.id.editTextSearchEvents);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String filteredText = et.getText().toString().toLowerCase(Locale.getDefault());
                filter(filteredText, eventList);

            }
        });
    }

    public void fillAdapter(List<EventModel> data) {
        adapter = new EventAdapter(c, R.layout.list_item_layout, data, email, userID, bundle);
        if (listViewEvents != null) {
            if (data != null) {
                listViewEvents.setAdapter(adapter);
            }
        }
    }

    public void filter(String charText, List<EventModel> list) {
        charText = charText.toLowerCase(Locale.getDefault());
        filteredList = new ArrayList();
        if (charText.length() == 0) {
            fillAdapter(list);
        } else {
            for (EventModel event : list) {
                if (event.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    filteredList.add(event);
                }
            }

            fillAdapter(filteredList);
        }
        adapter.notifyDataSetChanged();
    }
}
