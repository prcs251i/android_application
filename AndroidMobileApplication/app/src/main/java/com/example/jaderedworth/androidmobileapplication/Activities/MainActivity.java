package com.example.jaderedworth.androidmobileapplication.Activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.BaseActivity.BaseUIActivity;
import com.example.jaderedworth.androidmobileapplication.Fragments.BookingFragment;
import com.example.jaderedworth.androidmobileapplication.Fragments.EventsFragment;
import com.example.jaderedworth.androidmobileapplication.Fragments.RecommendedFragment;
import com.example.jaderedworth.androidmobileapplication.Fragments.UserAccountFragment;
import com.example.jaderedworth.androidmobileapplication.JSONModels.AuthenticationHolder;
import com.example.jaderedworth.androidmobileapplication.JSONModels.CustomerModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.R;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.Serializable;
import java.util.List;


public class MainActivity extends BaseUIActivity {

    private Boolean loggedInCheck = false;
    private Fragment fragment = null;
    private String title = null;
    private int targetFragment;
    private String targetType;
    private List<EventModel> eventList;
    private TextView txtWelcome;
    private CustomerModel customerModel;
    private GoogleApiClient client;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = new Bundle();
        bundle = getIntent().getExtras();

        try {
            targetType = bundle.getString("TARGET_FRAGMENT");
            Log.d("BundleTestMA", String.valueOf(bundle.getSerializable("EVENT")));
            if (transitionRequired(targetType)) {
                Log.d("MainAct", "temp");
                super.transitionToTarget(fragment, title, null, bundle);
            }
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
        customerModel = AuthenticationHolder.getInstance();
        setContentView(R.layout.fragment_home);
        if (customerModel != null) {
            Log.d("Cust", String.valueOf(customerModel));
            txtWelcome = (TextView) findViewById(R.id.textViewWelcome);
            String tempString = customerModel.getForename() + " " + customerModel.getSurname();
            Log.d("tempString", String.valueOf(tempString));
            txtWelcome.setText("Welcome " + tempString);
        }
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private boolean transitionRequired(String type) {
        boolean transReq = false;
        if (type != "Main") {
            switch (type) {
                case "Recommended": {
                    fragment = new RecommendedFragment();
                    title = "Recommended";
                    transReq = true;
                    break;
                }
                case "Events": {
                    fragment = new EventsFragment(this);
                    title = "Current Events";
                    transReq = true;
                    break;
                }
                case "Bookings": {
                    fragment = new BookingFragment(this);
                    title = "Your Bookings";
                    transReq = true;
                    break;
                }
                case "Account": {
                    fragment = new UserAccountFragment(this);
                    title = "Your Details";
                    transReq = true;
                    break;
                }
            }
        }
        return transReq;
    }
}


