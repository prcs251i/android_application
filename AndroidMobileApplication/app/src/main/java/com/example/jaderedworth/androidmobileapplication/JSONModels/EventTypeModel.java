package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;

/**
 * Created by jaderedworth on 24/04/2016.
 */
public class EventTypeModel implements Serializable {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    private int id;
    private String eventType;
}
