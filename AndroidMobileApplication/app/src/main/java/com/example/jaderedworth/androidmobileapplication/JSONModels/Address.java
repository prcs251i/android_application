package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;

public class Address implements Serializable {

    private String address_Line_1;
    private String address_Line_2;
    private String city;
    private String county;
    private String postcode;

    public Address() {

    }

    public Address(String addrLine1, String addrLine2, String addrCity, String addrCounty, String addrPostcode) {
        address_Line_1 = addrLine1;
        address_Line_2 = addrLine2;
        city = addrCity;
        county = addrCounty;
        postcode = addrPostcode;
    }

    public String getFormattedAddress() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getAddressLine1() {
        return this.address_Line_1;
    }

    public void setAddressLine1(String newAddrLine1) {
        this.address_Line_1 = newAddrLine1;
    }

    public String getAddressLine2() {
        return this.address_Line_2;
    }

    public void setAddressLine2(String newAddrLine2) {
        this.address_Line_2 = newAddrLine2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String newCity) {
        this.city = newCity;
    }

    public String getCounty() {
        return this.county;
    }

    public void setCounty(String newCounty) {
        this.county = newCounty;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public void setPostcode(String newPostcode) {
        this.postcode = newPostcode;
    }



}