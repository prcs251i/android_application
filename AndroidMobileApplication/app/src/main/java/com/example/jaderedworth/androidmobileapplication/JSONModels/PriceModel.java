package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;

/**
 * Created by jaderedworth on 24/04/2016.
 */
public class PriceModel implements Serializable {

    private int id;
    private int runId;
    private int tierId;
    //private TierModel Tier;
    private double price;
    private boolean seated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRunId() {
        return runId;
    }

    public void setRunId(int runId) {
        this.runId = runId;
    }

    public int getTierId() {
        return tierId;
    }

    public void setTierId(int tierId) {
        this.tierId = tierId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isSeated() {
        return seated;
    }

    public void setSeated(boolean seated) {
        this.seated = seated;
    }


}
