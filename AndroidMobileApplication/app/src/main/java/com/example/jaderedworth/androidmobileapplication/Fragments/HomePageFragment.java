package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.JSONModels.CustomerModel;
import com.example.jaderedworth.androidmobileapplication.R;

import java.text.SimpleDateFormat;

/**
 * Created by Andrew on 13/05/2016.
 * In theory, this is just a placeholder. Unless we decide to do anything this will remain blank.
 * Declared such that the home page can be navigated to without starting a new activity and potentially
 * causing memory leaks, as previously home button would just create a new activity without removing
 * any old ones. (Also bad because it was incredibly unreliable at passing data due to the multiple
 * potential sources of a home page creation intent)
 */
public class HomePageFragment extends Fragment {

   private Context c;

    public HomePageFragment() {
    }

    public HomePageFragment(Context context) {
        c = context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        return rootView;
    }
}
