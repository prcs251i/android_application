package com.example.jaderedworth.androidmobileapplication;

import android.os.AsyncTask;

import com.example.jaderedworth.androidmobileapplication.JSONModels.Address;
import com.example.jaderedworth.androidmobileapplication.JSONModels.Booking;
import com.example.jaderedworth.androidmobileapplication.JSONModels.CustomerModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventTypeModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.SeatedTicketModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.StandingTicketModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TicketModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TierModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jaderedworth on 06/05/2016.
 */

public class JSONBookingTask extends AsyncTask<String, String, ArrayList<Booking>> {

    ArrayList<Booking> bookingModelList;
    ArrayList<TicketModel> ticketModelList;

    public interface AsyncResponse {
        void processFinish(ArrayList<Booking> output);
    }

    public AsyncResponse delegate = null;

    public JSONBookingTask(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected ArrayList<Booking> doInBackground(String... params) {

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            String finalJSON = buffer.toString();
            JSONArray parentArray = new JSONArray(finalJSON);

            bookingModelList = new ArrayList();
            for (int i = 0; i < parentArray.length(); i++) {
                JSONObject finalObject = parentArray.getJSONObject(i);
                Booking booking = new Booking();
                booking.setBookingID(finalObject.getInt("ID"));
                booking.setRunID(finalObject.getInt("RUN_ID"));
                booking.setCustomer(finalObject.getInt("PERSON_ID"));
                String dateString = finalObject.getString("ORDER_DATE");
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date date = formatter.parse(dateString);
                booking.setDatePlaced(date);
                booking.setNoOfTickets(finalObject.getInt("QUANTITY"));
                booking.setTotalCost(finalObject.getDouble("TOTAL_COST"));
                ticketModelList = new ArrayList();
                for (int j = 0; j < finalObject.getJSONArray("TICKETS").length(); j++) {
                    JSONObject ticketObject = finalObject.getJSONArray("TICKETS").getJSONObject(j);
                    JSONObject tierObject = ticketObject.getJSONObject("TIER");
                    TierModel tier = new TierModel();
                    tier.setTierName(tierObject.getString("TIER_NAME"));
                    if (tierObject.getString("SEATED").contains("Y")) {
                        SeatedTicketModel seatedTicket = new SeatedTicketModel();
                        seatedTicket.setBookingID(ticketObject.getInt("BOOKING_ID"));
                        seatedTicket.setTicketRef(ticketObject.getInt("ID"));
                        seatedTicket.setSeatRow(ticketObject.getInt("SEAT_ROW"));
                        seatedTicket.setSeatNo(ticketObject.getInt("SEAT_NUM"));
                        seatedTicket.setAllocatedTierModel(tier);
                        ticketModelList.add(seatedTicket);
                    } else if (tierObject.getString("SEATED").contains("N")) {
                        StandingTicketModel standingTicket = new StandingTicketModel();
                        standingTicket.setBookingID(finalObject.getInt("ID"));
                        standingTicket.setTicketRef(ticketObject.getInt("BOOKING_ID"));
                        standingTicket.setAllocatedTierModel(tier);
                        ticketModelList.add(standingTicket);
                    }
                }
                booking.setTicketList(ticketModelList);

                JSONObject runObject = finalObject.getJSONObject("RUN");
                for(int k = 0; k < runObject.getJSONArray("PERFORMANCES").length(); k++) {
                    JSONObject performerObject = runObject.getJSONArray("PERFORMANCES").getJSONObject(k);
                    JSONObject actObject = performerObject.getJSONObject("ACT");
                    JSONObject typeObject = actObject.getJSONObject("TYPE");
                    EventTypeModel eventTypeModel = new EventTypeModel();
                    eventTypeModel.setEventType(typeObject.getString("NAME"));
                    booking.setEventTypeModel(eventTypeModel);
                }

                bookingModelList.add(booking);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bookingModelList;
    }

    @Override
    protected void onPostExecute(ArrayList<Booking> result) {
        delegate.processFinish(result);
    }

}