package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jaderedworth.androidmobileapplication.DataValidator;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R;// TODO: 04/05/2016

public class RegisterFragmentMain extends Fragment {

    private Context context;
    private EditText emailInput;
    private EditText passwordInput;
    private EditText confirmPasswordInput;
    private EditText forenameInput;
    private EditText surnameInput;
    private EditText homePhoneInput;
    private EditText mobilePhoneInput;

    private Button proceedButton;
    private Button backButton;

    private String email;
    private String password;
    private String confirmPass;
    private String forename;
    private String surname;
    private String homePhone;
    private String mobilePhone;

    private AlertDialog.Builder builder;

    public RegisterFragmentMain(Context c){
        this.context = c;
    }

    @Override
    public View onCreateView(final LayoutInflater i, ViewGroup vg, Bundle savedInstanceState ){
        super.onCreateView(i, vg, savedInstanceState);
        View rootView = i.inflate(R.layout.fragment_register_main, vg, false);

        emailInput = (EditText)  rootView.findViewById(R.id.editTextEmail);
        passwordInput = (EditText) rootView.findViewById(R.id.editTextPassword);
        confirmPasswordInput = (EditText) rootView.findViewById(R.id.editTextConfirmPassword);

        forenameInput = (EditText) rootView.findViewById(R.id.editTextForename);
        surnameInput = (EditText) rootView.findViewById(R.id.editTextSurname);
        homePhoneInput = (EditText) rootView.findViewById(R.id.editTextHomePhone);
        mobilePhoneInput = (EditText) rootView.findViewById(R.id.editTextMobilePhone);

        proceedButton = (Button) rootView.findViewById(R.id.btnCompleteRegistrationStepOne);
        backButton = (Button) rootView.findViewById(R.id.buttonBackRegistrationStepOne);

        proceedButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                email = emailInput.getText().toString();
                Log.d("email", ""+email);
                password = passwordInput.getText().toString();
                Log.d("password", ""+password);
                confirmPass = confirmPasswordInput.getText().toString();
                Log.d("password2", ""+confirmPass);

                forename = forenameInput.getText().toString();
                Log.d("forename", ""+forename);
                surname = surnameInput.getText().toString();
                Log.d("surname", ""+surname);
                homePhone = homePhoneInput.getText().toString();
                Log.d("homephone", ""+homePhone);
                mobilePhone = mobilePhoneInput.getText().toString();
                Log.d("mobilephone", ""+mobilePhone);

                if(ValidInput()){
                    RegisterFragmentShippingInfo targetFragment = new RegisterFragmentShippingInfo();

                    Bundle userData  = new Bundle();
                    userData.putString("EMAIL", email);
                    userData.putString("PASSWORD_HASH", password);
                    userData.putString("FORENAME", forename);
                    userData.putString("SURNAME", surname);
                    userData.putString("HOME_PHONE", homePhone);
                    userData.putString("MOB_PHONE", mobilePhone);

                    targetFragment.setArguments(userData);
                    FragmentTransaction t = getFragmentManager().beginTransaction();
                    t.replace(R.id.fragment_login_main, targetFragment);
                    t.addToBackStack(null);
                    getFragmentManager().popBackStack();

                    t.commit();
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public Boolean ValidInput(){
        Boolean inputIsValid = false;

        DataValidator dataValidator = new DataValidator(this.getContext());
        Boolean validPassword = false;
        Boolean validEmail;
        Boolean validForename;
        Boolean validSurname;
        Boolean validHomePhone;
        Boolean validMobilePhone;

        dataValidator.setArguments(surname, "Surname");
        validSurname= dataValidator.fieldIsNotEmpty()&&dataValidator.alphabeticStringIsValid();
        dataValidator.setArguments(forename, "Forename");
        validForename = dataValidator.fieldIsNotEmpty()&&dataValidator.alphabeticStringIsValid();

        dataValidator.setArguments(password, "Password");
        if (password.matches(confirmPass)) {
            validPassword = dataValidator.passwordIsValid();
        }
        else if(!password.matches(confirmPass)){
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Password does not match").setTitle("Attention");
            AlertDialog dialog = builder.create();
            dialog.show();
            validPassword = false;
        }
        dataValidator.setArguments(email, "Email");
        validEmail = dataValidator.fieldIsNotEmpty()&&dataValidator.emailIsValid();
        dataValidator.setArguments(homePhone, "Home Phone Number");
        validHomePhone= dataValidator.numericStringIsValid();
        dataValidator.setArguments(mobilePhone, "Mobile Phone Number");
        validMobilePhone= dataValidator.numericStringIsValid();

        if (validPassword&&validEmail&&validForename&&validSurname&&validHomePhone&&validMobilePhone){
            inputIsValid = true;
        }
        else{
           inputIsValid = false;
        }
        return inputIsValid;
    }


}
