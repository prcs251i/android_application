package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.example.jaderedworth.androidmobileapplication.Adapters.EventAdapter;
import com.example.jaderedworth.androidmobileapplication.JSONModels.Booking;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventTypeModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.R;// TODO: 04/05/2016

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class RecommendedFragment extends Fragment implements Serializable{

    private Bundle bundle;
    private EventAdapter adapter;
    private List<EventModel> eventList;
    private List<Booking> bookingList;
    ArrayList<EventModel> filteredList;
    private List<EventModel> recommendedEvents;
    private ListView listViewRecommendedEvents;
    private String email;
    private int userID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_recommended, container, false);

        bundle = this.getArguments();
        if (bundle != null) {
            eventList = (List<EventModel>) bundle.getSerializable("EVENT");
            if(bundle.getSerializable("BOOKINGS")!=null){
                bookingList = (List<Booking>) bundle.getSerializable("BOOKINGS");}
            email = bundle.getString("EMAIL");
            userID = bundle.getInt("USER_ID", 0);
        }

        listViewRecommendedEvents = (ListView) rootView.findViewById(R.id.listViewRecommendedEvents);
        setRecommendedEvents();
        doSearch(rootView);
        return rootView;
    }

    private void setRecommendedEvents() {

        recommendedEvents = new ArrayList<>();
        List<String> events = new ArrayList();

        if(bookingList != null) {
            for (int i = 0; i < bookingList.size(); i++) {
                if (!events.contains(bookingList.get(i).getEventTypeModel().getEventType())) {
                    events.add(bookingList.get(i).getEventTypeModel().getEventType());
                }
            }
        }
        for (EventModel event : eventList) {
            if (events.contains(event.getEventType())){
                recommendedEvents.add(event);
            }
        }
        fillAdapter(recommendedEvents);
    }

    private void doSearch(View rootView) {
        final EditText et = (EditText) rootView.findViewById(R.id.editTextSearchRecommendedEvents);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String filteredText = et.getText().toString().toLowerCase(Locale.getDefault());
                filter(filteredText, recommendedEvents);

            }
        });
    }

    public void fillAdapter(List<EventModel> data) {
        adapter = new EventAdapter(getContext(), R.layout.list_item_layout, data, email, userID, bundle);
        if (listViewRecommendedEvents != null) {
            if (data != null) {
                listViewRecommendedEvents.setAdapter(adapter);
            }
        }
    }

    public void filter(String charText, List<EventModel> list) {
        charText = charText.toLowerCase(Locale.getDefault());
        filteredList = new ArrayList();
        if (charText.length() == 0) {
            fillAdapter(list);
        } else {
            for (EventModel event : list) {
                if (event.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    filteredList.add(event);
                }
            }

            fillAdapter(filteredList);
        }
        adapter.notifyDataSetChanged();
    }
}