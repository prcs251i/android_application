package com.example.jaderedworth.androidmobileapplication.JSONModels;


import java.io.Serializable;

public class SeatedTicketModel extends TicketModel implements Serializable {
    
    private int seatRow;
    private int seatNumber;

    public SeatedTicketModel() {
        
    }
    
    public SeatedTicketModel(int ref, int bookingRef, VenueModel venue, TierModel tierModel, double price, int row, int seatNo) {
        super(ref, bookingRef, venue, tierModel, price);
        seatRow = row;
        seatNumber = seatNo;
    }
    
    public int getSeatNo() {
        return this.seatNumber;
    }
    
    public void setSeatNo(int newSeatRow) {
        this.seatNumber = newSeatRow;
    }
    
    public int getSeatRow() {
        return this.seatRow;
    }
    
    public void setSeatRow(int newSeatRow) {
        this.seatRow = newSeatRow;
    }

}
