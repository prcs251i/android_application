package com.example.jaderedworth.androidmobileapplication.BaseActivity;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.Activities.MainActivity;
import com.example.jaderedworth.androidmobileapplication.Fragments.BookingFragment;
import com.example.jaderedworth.androidmobileapplication.Fragments.EventsFragment;
import com.example.jaderedworth.androidmobileapplication.Fragments.HomePageFragment;
import com.example.jaderedworth.androidmobileapplication.Fragments.RecommendedFragment;
import com.example.jaderedworth.androidmobileapplication.Fragments.UserAccountFragment;
import com.example.jaderedworth.androidmobileapplication.JSONBookingTask;
import com.example.jaderedworth.androidmobileapplication.JSONModels.Booking;
import com.example.jaderedworth.androidmobileapplication.JSONModels.CustomerModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONTaskEvent;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R;// TODO: 04/05/2016

import java.io.Serializable;
import java.net.CookieManager;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 18/03/2016.
 */
public abstract class BaseUIActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Serializable {

    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;
    public int userID;
    private String email;
    private String password;
    private ArrayList<EventModel> eventList = new ArrayList();//Todo
    private ArrayList<Booking> bookingList = new ArrayList();//Todo
    private ArrayList<ImageView> imageList;
    private CustomerModel customer;


    private CookieManager cookieManager;

    @Override
    public void setContentView(int layoutResID){
        Log.d("SetContentView", "Executing");
        DrawerLayout fullView = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_main, null);
        FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.content_frame);

        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);
        toolbar = (Toolbar) findViewById(R.id.toolbarLogin);
        setSupportActionBar(toolbar);
        //setTitle("Activity Title");

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        if(navigationView!=null){
            Log.d("NavView", "Setting click listener");
            navigationView.setNavigationItemSelectedListener(this);
        }

        toggle = new ActionBarDrawerToggle(this, fullView, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);

        if (fullView ==null){
            Log.d("fV", "null");
        }
        if (toggle !=null) {
            fullView.addDrawerListener(toggle);

        }

        if (useToolbar())
        {
            setSupportActionBar(toolbar);
            //setTitle("Activity Title");
        }
        else
        {
            toolbar.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        Log.d("Extras", String.valueOf(getIntent().getExtras()));
        email = getIntent().getStringExtra("USERNAME");
        password = getIntent().getStringExtra("PASSWORD");
        userID = getIntent().getIntExtra("USER_ID", 0);
        customer = (CustomerModel) getIntent().getSerializableExtra("CUSTOMER");
        if (eventList.isEmpty()||bookingList.isEmpty()) {
            getData();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item){
        Fragment fragment = null;
        String targetTitle = null;
        String targetFragmentType = null;

        int id = item.getItemId();
        switch(id){
            case R.id.nav_recommended:{
                fragment = new RecommendedFragment();
                targetTitle = "TicketStack";
                targetFragmentType = "Recommended";
                break;
            }
            case R.id.nav_events:{
                fragment = new EventsFragment(this);
                targetTitle = "TicketStack";
                targetFragmentType = "Events";
                break;
            }
            case R.id.nav_my_bookings:{
                fragment = new BookingFragment(this);
                targetTitle = "TicketStack";
                targetFragmentType = "Bookings";
                break;
            }
            case R.id.my_account:{
                fragment = new UserAccountFragment(this);
                targetTitle = "TicketStack";
                targetFragmentType = "Your Details";
                break;
            }
        }
        transitionToTarget(fragment, targetTitle, targetFragmentType, null);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu m){
        getMenuInflater().inflate(R.menu.main, m);

        return super.onCreateOptionsMenu(m);

    }

    @Override
    public void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (id == R.id.button_home) {
            transitionToTarget(new HomePageFragment(), "Home", "Main", null);
            return true;
        }
        if (id == R.id.logout){
            startActivity(new Intent(this, LoginActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    protected boolean useToolbar(){
        return true;
    }

    protected void transitionToTarget(Fragment f, String title, String fragmentType, Bundle dataToPass){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = null;

        Fragment fragment = f;
        String targetTitle = title;


        if (dataToPass ==null){
            dataToPass = new Bundle();
            Log.d("NullCheckTTT", "Null");
        }

        //Set Bundle values
        if(getIntent().getExtras()!= null){
            dataToPass.putAll(getIntent().getExtras());
        }

        if(!eventList.isEmpty()){
            Log.d("EventListTTT", String.valueOf(eventList));
            dataToPass.putSerializable("EVENT", (Serializable) eventList);
        }
        if (!bookingList.isEmpty()){
            Log.d("BookingListTTT", String.valueOf(bookingList));
            dataToPass.putSerializable("BOOKINGS", (Serializable) bookingList);
        }
        if (customer !=null){
            dataToPass.putSerializable("CUSTOMER", (Serializable) customer);
        }
        if (email !=null){
            dataToPass.putString("EMAIL", email);
        }
        if (password!=null){
            dataToPass.putString("PASSWORD", password);
        }

        dataToPass.putInt("USER_ID", userID);

        Log.d("BundleTestTTT", String.valueOf(dataToPass.getSerializable("EVENT")));

        if (!inMainActivity()){
            Intent i = new Intent(this, MainActivity.class);
            dataToPass.putString("TARGET_FRAGMENT", fragmentType);
            i.putExtras(dataToPass);
            startActivity(i);
        }

        // If fragment is no longer null then a target has been declared
        //If in main activity no need to nav to new fragment
        if (inMainActivity()&&fragment!=null){
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);

            this.setTitle(targetTitle);
            fragment.setArguments(dataToPass);
            fragmentTransaction.commit();
        }


    }

    private boolean inMainActivity(){
        String activityName;
        boolean inMain=false;
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(1);
        activityName = taskInfo.get(0).topActivity.getClassName();

        if (activityName.matches("(.*)MainActivity")){
            inMain = true;
        }

        return inMain;
    }

    protected void getData() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(1);
        URL url;
        try {
            url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/events");
            new JSONTaskEvent(new JSONTaskEvent.AsyncResponse() {
                @Override
                public void processFinish(ArrayList<EventModel> output) {
                    if (output != null) {
                        eventList = output;
                    }
                }
            }).execute(url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        final URL bookingUrl;
        try {
            bookingUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/bookings/myBookings/" + userID);
            new JSONBookingTask(new JSONBookingTask.AsyncResponse() {
                @Override
                public void processFinish(ArrayList<Booking> output) {
                    bookingList = output;
                }
            }).execute(bookingUrl.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

}
