package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.JSONPostHandler;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterFragmentConfirm extends Fragment {

    private Bundle userData;
    TextView userDetails;
    private String JSONData;
    private TextView email;
    private TextView forename;
    private TextView surname;
    private String password;
    private TextView homePhone;
    private TextView mobPhone;
    private TextView cardType;
    private TextView cardNo;
    private TextView expiryDate;
    private TextView securityCode;
    private TextView addressLine1;
    private TextView addressLine2;
    private TextView city;
    private TextView county;
    private TextView postcode;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_register_confirm, container, false);

        Button finishRegistration = (Button) rootView.findViewById(R.id.btnFinishRegistration);
        Button backButton = (Button) rootView.findViewById(R.id.buttonBackRegistrationFinish);
        userData = this.getArguments();

        email = (TextView) rootView.findViewById(R.id.textViewRegEMail);
        forename = (TextView) rootView.findViewById(R.id.textViewRegForename);
        surname = (TextView) rootView.findViewById(R.id.textViewRegSurname);
        homePhone = (TextView) rootView.findViewById(R.id.textViewRegHomePhone);
        mobPhone = (TextView) rootView.findViewById(R.id.textViewRegMobPhone);
        cardType = (TextView) rootView.findViewById(R.id.textViewRegCardType);
        cardNo = (TextView) rootView.findViewById(R.id.textViewRegCardNumber);
        expiryDate = (TextView) rootView.findViewById(R.id.textViewRegExpiryDate);
        securityCode = (TextView) rootView.findViewById(R.id.textViewRegSecurityCode);
        addressLine1 = (TextView) rootView.findViewById(R.id.textViewRegAddressLine1);
        addressLine2 = (TextView) rootView.findViewById(R.id.textViewRegAddressLine2);
        city = (TextView) rootView.findViewById(R.id.textViewRegCity);
        county = (TextView) rootView.findViewById(R.id.textViewRegCounty);
        postcode = (TextView) rootView.findViewById(R.id.textViewRegPostcode);

        password = userData.getString("PASSWORD_HASH");
        email.append(userData.getString("EMAIL"));
        forename.append((userData.getString("FORENAME")));
        surname.append(userData.getString("SURNAME"));
        if (!userData.getString("HOME_PHONE").isEmpty()) {
            homePhone.append(userData.getString("HOME_PHONE"));
        }
        if (!userData.getString("MOB_PHONE").isEmpty()) {
            mobPhone.append(userData.getString("MOB_PHONE"));
        }
        cardType.append(userData.getString("CARD_TYPE"));
        cardNo.append(userData.getString("CARD_NUMBER"));
        expiryDate.append(userData.getString("EXPIRY_DATE"));
        securityCode.append(String.valueOf(userData.getInt("SECURITY_CODE")));
        addressLine1.append(userData.getString("ADDRESS_LINE_1"));
        if (!userData.getString("ADDRESS_LINE_2").isEmpty()) {
            addressLine2.append(userData.getString("ADDRESS_LINE_2"));
        }
        city.append(userData.getString("CITY"));
        county.append(userData.getString("COUNTY"));
        postcode.append(userData.getString("POSTCODE"));
        try {
            JSONData = populateJSONObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        finishRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JSONPostHandler(new JSONPostHandler.AsyncPostResponse() {
                    @Override
                    public void processFinish(String output) {
                        AlertDialog.Builder builder;
                        if (output != null) {
                            if (output.contains("20")) {
                                builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage("You have been successfully registered!").setTitle("Congratulations!");
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                            } else {
                                builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage("Sorry, we were unable to register you at this time. Please try again later").setTitle("Warning!");
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    }
                }).execute(String.valueOf(JSONData), "http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/customers");
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragmentPaymentInfo targetFragment = new RegisterFragmentPaymentInfo();
                FragmentTransaction t = getFragmentManager().beginTransaction();
                t.replace(R.id.fragment_login_main, targetFragment);
                t.addToBackStack(null);
                t.commit();
            }
        });
        return rootView;
    }

    public String populateJSONObject()throws JSONException{
        JSONObject tempJSON = new JSONObject();

        tempJSON.put("FORENAME", userData.getString("FORENAME"));
        tempJSON.put("SURNAME", userData.getString("SURNAME"));
        tempJSON.put("EMAIL", userData.getString("EMAIL"));
        tempJSON.put("PASSWORD_HASH", userData.getString("PASSWORD_HASH"));
        tempJSON.put("ADDRESS_LINE_1", userData.getString("ADDRESS_LINE_1"));
        tempJSON.put("CITY", userData.getString("CITY"));
        tempJSON.put("COUNTY", userData.getString("COUNTY"));
        tempJSON.put("POSTCODE", userData.getString("POSTCODE"));
        tempJSON.put("CARD_TYPE", userData.getString("CARD_TYPE"));
        tempJSON.put("CARD_NUMBER", userData.getString("CARD_NUMBER"));
        tempJSON.put("EXPIRY_DATE", userData.getString("EXPIRY_DATE"));
        tempJSON.put("SECURITY_CODE", String.valueOf(userData.getInt("SECURITY_CODE")));

        if (!userData.getString("ADDRESS_LINE_2").isEmpty()) {
            tempJSON.put("ADDRESS_LINE_2", userData.getString("ADDRESS_LINE_2"));
        }
        if (!userData.getString("HOME_PHONE").isEmpty()) {
            tempJSON.put("HOME_PHONE", userData.getString("HOME_PHONE"));
        }
        if (!userData.getString("MOB_PHONE").isEmpty()) {
            tempJSON.put("MOB_PHONE", userData.getString("MOB_PHONE"));
        }
        return tempJSON.toString();
    }
}
