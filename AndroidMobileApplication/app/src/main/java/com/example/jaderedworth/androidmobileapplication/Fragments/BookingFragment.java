package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.Adapters.BookingAdapter;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.R; // TODO: 04/05/2016
import com.example.jaderedworth.androidmobileapplication.JSONModels.Booking;

import java.util.List;

public class BookingFragment extends Fragment {

    private ListView listUserBookings;
    private List<Booking> bookingList;
    private List<EventModel> eventList;
    private BookingAdapter adapter;
    private Bundle bundle;
    private String email;
    private int loggedInUserID;
    private Context c;
    private TextView noBookingMessage;
    public BookingFragment() {
    }

    public BookingFragment(Context context) {
        c = context;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_booking, container, false);
        noBookingMessage = (TextView) rootView.findViewById(R.id.textViewNoBookings);

        bundle = this.getArguments();
        if (bundle != null) {
            if(bundle.getSerializable("EVENT")!=null){
            eventList = (List<EventModel>) bundle.getSerializable("EVENT");}
            if(bundle.getSerializable("BOOKINGS")!=null){
            bookingList = (List<Booking>) bundle.getSerializable("BOOKINGS");}
            email = bundle.getString("EMAIL");
            loggedInUserID = bundle.getInt("USER_ID", 0);
        }

        listUserBookings = (ListView) rootView.findViewById(R.id.listViewBooking);

        if(bookingList!=null){
            fillAdapter(bookingList);
        } else {
            noBookingMessage.setText("You have no bookings");
        }

        return rootView;
    }


    public void fillAdapter(List<Booking> data) {
        adapter = new BookingAdapter(c, R.layout.list_booking_layout, data, eventList, bundle);
        if (listUserBookings != null) {
            if (data != null) {
                listUserBookings.setAdapter(adapter);
            }
        }
    }
}