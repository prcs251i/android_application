package com.example.jaderedworth.androidmobileapplication.JSONModels;

import java.io.Serializable;

public abstract class TicketModel implements Serializable {
        
    private int ticketRef;
    private int bookingID;
    private TierModel allocatedTierModel;
    private double ticketPrice;
    
    public TicketModel() {
        
    }
    
    public TicketModel(int ref, int bookingRef, VenueModel venue, TierModel tierModel, double price) {
        ticketRef = ref;
        bookingID = bookingRef;
        allocatedTierModel = tierModel;
        ticketPrice = price;       
    }

    public int getTicketRef() {
        return this.ticketRef;
    }
    
    public void setTicketRef(int newTicketRef) {
        this.ticketRef = newTicketRef;
    }
    
    public int getBookingID() {
        return this.bookingID;
    }
    
    public void setBookingID(int newBookingID) {
        this.bookingID = newBookingID;
    }

    public TierModel getAllocatedTierModel() {
        return this.allocatedTierModel;
    }
    
    public void setAllocatedTierModel(TierModel newTierModel) {
        this.allocatedTierModel = newTierModel;
    }
    
}
