package com.example.jaderedworth.androidmobileapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.jaderedworth.androidmobileapplication.Activities.RunActivity;
import com.example.jaderedworth.androidmobileapplication.R;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;

public class EventAdapter extends ArrayAdapter {

    private ArrayList<EventModel> eventModelList;
    private int resource;
    private Context mContext;
    private String email;
    private int userID;
    private Bundle persistedData;

    public EventAdapter(Context context, int resource, List<EventModel> objects, String email, int userID, Bundle bundle) {
        super(context, resource, objects);
        eventModelList = (ArrayList<EventModel>) objects;
        this.resource = resource;
        this.mContext = context;
        this.email = email;
        this.userID = userID;
        this.persistedData = bundle;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item_layout, null);

            viewHolder.eventTitle = (TextView) convertView.findViewById(R.id.textViewEventTitle);
            viewHolder.eventType = (TextView) convertView.findViewById(R.id.textViewEventType);
            viewHolder.eventAge = (TextView) convertView.findViewById(R.id.textViewEventAgeRating);
            viewHolder.eventRating = (RatingBar) convertView.findViewById(R.id.ratingBarEvents);
            viewHolder.eventImage = (ImageView) convertView.findViewById(R.id.imageViewEvents);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.eventTitle.setText(eventModelList.get(position).getTitle());
        viewHolder.eventType.setText("Event Type: " + eventModelList.get(position).getEventType());
        viewHolder.eventAge.setText("Age Rating: " + eventModelList.get(position).getAge());
        Picasso.with(mContext).load(eventModelList.get(position).getImage()).into(viewHolder.eventImage);

        viewHolder.eventRating.setRating((float) eventModelList.get(position).getRating());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final List<RunModel> runsList = eventModelList.get(position).getRuns();
                Intent intent = new Intent(mContext, RunActivity.class);

                intent.putExtras(persistedData);
                intent.putExtra("RUN_LIST", (Serializable) runsList);
                intent.putExtra("EVENT", eventModelList.get(position));
                intent.putExtra("EMAIL", email);
                intent.putExtra("USER_ID", userID);
                mContext.startActivity(intent);
                Log.d("CLICK", "onClick: OPEN Activity");
            }
        });

        return convertView;
    }

    public class ViewHolder {
        TextView eventTitle;
        TextView eventType;
        TextView eventAge;
        RatingBar eventRating;
        ImageView eventImage;
    }
}