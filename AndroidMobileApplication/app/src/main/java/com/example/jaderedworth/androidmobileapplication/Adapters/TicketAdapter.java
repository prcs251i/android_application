package com.example.jaderedworth.androidmobileapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.Activities.TicketDetailsActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.SeatedTicketModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TicketModel;
import com.example.jaderedworth.androidmobileapplication.R;

import java.net.CookieHandler;
import java.util.List;

/**
 * Created by jaderedworth on 09/05/2016.
 */
public class TicketAdapter extends ArrayAdapter {

    private List<TicketModel> ticketModelList;
    private int resource;
    private Context context;
    private LayoutInflater inflater;
    private Bundle persistedData;

    public TicketAdapter(Context context, int resource, List<TicketModel> objects, Bundle bundle) {
        super(context, resource, objects);
        ticketModelList = objects;
        this.resource = resource;
        this.context = context;
        this.persistedData = bundle;
        inflater = (LayoutInflater) getContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return ticketModelList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_ticket_layout, null);

            viewHolder.ticketRefNo = (TextView) convertView.findViewById(R.id.textViewTicketRefNo);
            viewHolder.tierName = (TextView) convertView.findViewById(R.id.textViewTicketTier);
            viewHolder.seatRow = (TextView) convertView.findViewById(R.id.textViewtTicketSeatRow);
            viewHolder.seatNo = (TextView) convertView.findViewById(R.id.textViewTicketSeatNo);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.ticketRefNo.setText("Ticket Reference: " + String.valueOf(ticketModelList.get(position).getTicketRef()));
        viewHolder.tierName.setText(ticketModelList.get(position).getAllocatedTierModel().getTierName());
        if(ticketModelList.get(position) instanceof SeatedTicketModel){
            SeatedTicketModel seatedTicketModel = (SeatedTicketModel) ticketModelList.get(position);
            viewHolder.seatRow.setText(String.valueOf(seatedTicketModel.getSeatRow()));
            viewHolder.seatNo.setText(String.valueOf(seatedTicketModel.getSeatNo()));
        } else {
            viewHolder.seatRow.setText("N/A");
            viewHolder.seatNo.setText("N/A");

        }
        return convertView;
    }

    public class ViewHolder {
        TextView ticketRefNo;
        TextView tierName;
        TextView seatRow;
        TextView seatNo;
    }
}
