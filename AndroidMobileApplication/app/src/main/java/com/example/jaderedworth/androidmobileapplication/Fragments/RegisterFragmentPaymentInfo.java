package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class RegisterFragmentPaymentInfo extends Fragment {

    private Spinner creditCardSpinner;
    private EditText cardNumberInput;
    private EditText expiryDateInput;
    private EditText securityCodeInput;
    private Button proceedButton;
    private Button backButton;

    private String cardType;
    private String cardNumber;
    private Date expiryDate;
    private int securityCode;

    private Bundle oldUserData;
    private Calendar c = Calendar.getInstance();
    private AlertDialog.Builder builder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_register_payment_info, container, false);

        oldUserData = this.getArguments();

        findInputFields(rootView);

        ArrayAdapter<CharSequence> a = ArrayAdapter.createFromResource(this.getActivity(), R.array.credit_card_array, android.R.layout.simple_spinner_item);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        creditCardSpinner.setAdapter(a);
        creditCardSpinner.setPrompt("Please select your Card Type");

        proceedButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                readUserInput();

                if (isValidInput()) {
                    Bundle userData;
                    userData = prepareDataToPass(oldUserData);
                    RegisterFragmentConfirm targetFragment = new RegisterFragmentConfirm();
                    targetFragment.setArguments(userData);
                    FragmentTransaction t = getFragmentManager().beginTransaction();
                    t.replace(R.id.fragment_login_main, targetFragment);
                    t.addToBackStack(null);
                    t.commit();
                }

            }
        });



        expiryDateInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragmentShippingInfo targetFragment = new RegisterFragmentShippingInfo();
                FragmentTransaction t = getFragmentManager().beginTransaction();
                t.replace(R.id.fragment_login_main, targetFragment);
                t.addToBackStack(null);
                t.commit();
            }
        });

        return rootView;
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, monthOfYear);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDateInput();
        }
    };

    private void updateDateInput(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy");
        expiryDateInput.setText(sdf.format(c.getTime()));
    }

    public void findInputFields(View tempView){
        creditCardSpinner = (Spinner) tempView.findViewById(R.id.spinnerCardType);
        cardNumberInput = (EditText)tempView.findViewById(R.id.editTextCardNumber);
        expiryDateInput = (EditText)tempView.findViewById(R.id.editTextExpiryDate);
        securityCodeInput = (EditText) tempView.findViewById(R.id.editTextSecurityCode);
        proceedButton = (Button) tempView.findViewById(R.id.btnCompleteRegistrationStepThree);
        backButton = (Button) tempView.findViewById(R.id.buttonBackRegistrationStepThree);

        expiryDateInput.setFocusable(false);
    }

    public void readUserInput(){
        cardType = creditCardSpinner.getSelectedItem().toString();
        cardNumber = cardNumberInput.getText().toString();

        try{
            if(securityCodeInput.getText().toString()!=null){
                if(securityCodeInput.getText().toString().length()==3||securityCodeInput.getText().toString().length()==4){
                    securityCode = Integer.parseInt(securityCodeInput.getText().toString());
                } else {
                    Toast.makeText(getActivity(), "Security Number invalid, please enter a valid Security Number", Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(getActivity(), "Security Number is mandatory, please enter a Security Number", Toast.LENGTH_LONG).show();
            }
        }catch (NumberFormatException e){
            Toast.makeText(getActivity(), "Security Number must be numeric, please enter a valid Security Number", Toast.LENGTH_LONG).show();
        }

        SimpleDateFormat s = new SimpleDateFormat("dd/MMM/yy");
        try {
            expiryDate = s.parse(expiryDateInput.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public Boolean isValidInput() {
        Boolean inputIsValid = false;

        Boolean validCardNumber = false;
        Boolean validExpiryDate;
        Date currentDate = new Date();
        String dateIntermediate;
        Calendar c = Calendar.getInstance();

        if (!cardNumber.isEmpty()) {
            validCardNumber = true;
        } else if (cardNumber.isEmpty()) {
            validCardNumber = false;
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Card Number is mandatory, please enter a Card Number").setTitle("Attention");
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        SimpleDateFormat s = new SimpleDateFormat("dd/MMM/yy");
        dateIntermediate = s.format(c.getTime());
        try {
            currentDate = s.parse(dateIntermediate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(expiryDate != null) {
            if (expiryDate.after(currentDate)) {
                validExpiryDate = true;
            } else {
                validExpiryDate = false;
                builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Card has expired, please use a Card that is within date, or check your given expiration is correct").setTitle("Attention");
                AlertDialog dialog = builder.create();
                dialog.show();
            }

            if (validExpiryDate && validCardNumber) {
                inputIsValid = true;
            }
        } else {
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Please enter an Expiry Date").setTitle("Attention");
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        return inputIsValid;
    }

    public Bundle prepareDataToPass(Bundle previousUserData){
        Bundle newBundle = new Bundle();
        newBundle.putAll(previousUserData);
        newBundle.putString("CARD_TYPE", cardType);
        newBundle.putString("CARD_NUMBER", cardNumber);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String formattedDate = format.format(expiryDate);
        newBundle.putString("EXPIRY_DATE", formattedDate);
        newBundle.putInt("SECURITY_CODE", securityCode);
        return newBundle;
    }


}
