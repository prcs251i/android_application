package com.example.jaderedworth.androidmobileapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.jaderedworth.androidmobileapplication.Adapters.BookingAdapter;

/**
 * Created by Andrew on 15/05/2016.
 */
public class DataValidator {
    protected String alertTitle = "Invalid Field:";
    protected String mandatoryMessage = " is mandatory, please enter a valid ";
    protected String invalidMessage = " you provided is not recognised as valid, please input a valid ";
    protected AlertDialog.Builder builder;
    protected String testString;
    protected String inputType;
    private AlertDialog alertDialog;
    private String hint;
    private Context context;


    public DataValidator(Context context){
        this.context = context;

    }

    public Boolean addressIsValid(){
        Boolean validString = false;
        String tempString = testString.replaceAll("\\s", "");

        this.setTestString(tempString);
        validString = alphanumericStringIsValid();
        return validString;
    }

    public Boolean alphabeticStringIsValid(){
        Boolean validString = false;
        if(testString.matches("^[a-zA-Z]*$")){
            validString = true;
        }
        if (!validString){
            showInvalidAlert(context);
        }
        return validString;
    }

    public Boolean alphanumericStringIsValid(){
        Boolean validString = false;

        if(testString.matches("^[\\p{Alnum}]*$")||testString.isEmpty()){
            validString = true;
        }
        if (!validString){
            showInvalidAlert(context);
        }
        return validString;
    }

    public Boolean emailIsValid(){
        Boolean validString = true; //Initialised as true since only retains true value if all strings pass check
        String[] splitStrings = testString.split(("@"));

        for (String sample : splitStrings){
            if(sample.length()==0||sample.contains(" ")||sample.contains("\\")){
                validString = false;
            }
        }

        if (!validString){
            showInvalidAlert(context);
        }

        return validString;
    }


    public Boolean fieldIsNotEmpty(){
        Boolean validString = true;

        if (testString.isEmpty()){
            showMandatoryMissingAlert(context);
            validString = false;
        }

        return validString;
    }

    public Boolean numericStringIsValid(){
        Boolean validString = false;
        if(testString.matches("\\d+")||testString.isEmpty()){
            validString = true;
        }
        if (!validString){
            hint = "Input can only be numerical eg: 0-9";
            showInvalidAlert(context);
        }
        return validString;
    }

    public Boolean passwordIsValid(){
        Boolean validString = true;
        if (testString.length() < 6) {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle(alertTitle);
            alertDialog.setMessage("Password must be at least 6 characters in length");
            alertDialog.show();
            validString = false;
        }
        return validString;
    }

    private void showInvalidAlert(Context context) {
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(alertTitle);
        alertDialog.setMessage("The " + inputType + invalidMessage+ inputType + "\n" + hint);
        alertDialog.show();
    }

    private void showMandatoryMissingAlert(Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(alertTitle);
        alertDialog.setMessage(inputType+mandatoryMessage+inputType);
        alertDialog.show();
    }

    public void setArguments(String testString, String inputType){
        this.testString=testString;
        this.inputType = inputType;
    }

    public String getTestString() {
        return testString;
    }

    public void setTestString(String testString) {
        this.testString = testString;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }
}
