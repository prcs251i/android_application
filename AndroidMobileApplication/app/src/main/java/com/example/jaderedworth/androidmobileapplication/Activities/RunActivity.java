package com.example.jaderedworth.androidmobileapplication.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;

import com.example.jaderedworth.androidmobileapplication.Adapters.RunsAdapter;
import com.example.jaderedworth.androidmobileapplication.AsyncLoginTask;
import com.example.jaderedworth.androidmobileapplication.BaseActivity.BaseUIActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R; // TODO: 04/05/2016


import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;

public class RunActivity extends BaseUIActivity implements Serializable {

    ArrayList<RunModel> runList;
    ArrayList<RunModel> filteredList;
    EventModel eventModel;
    ListView listViewRuns;
    private int userID;
    private String email;
    RunsAdapter adapter;
    private Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_runs);

        bundle = getIntent().getExtras();
        // cookieManager = cookieManager.getInstance();

        runList = (ArrayList<RunModel>) getIntent().getSerializableExtra("RUN_LIST");
        eventModel = (EventModel) getIntent().getSerializableExtra("EVENT");
        userID = getIntent().getIntExtra("USER_ID", 0);
        email = getIntent().getStringExtra("EMAIL");
        listViewRuns = (ListView) findViewById(R.id.listViewRuns);
        fillAdapter(runList);
    }

    public void fillAdapter(List<RunModel> data) {
        adapter = new RunsAdapter(this, R.layout.list_runs_layout, runList, eventModel, email, userID, bundle);
        if (listViewRuns != null) {
            if (data != null) {
                listViewRuns.setAdapter(adapter);
                Log.d("onPostExecute: ", "listview !null");
            } else {
                Log.d("onPostExecute", "fillAdapter: data is null");
            }
        } else if (listViewRuns == null) {
            Log.d("onPostExecute: ", "listview null");
        }
    }
}