package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jaderedworth.androidmobileapplication.Activities.MainActivity;
import com.example.jaderedworth.androidmobileapplication.AsyncLoginTask;
import com.example.jaderedworth.androidmobileapplication.BaseActivity.BaseUIActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.AuthenticationHolder;
import com.example.jaderedworth.androidmobileapplication.JSONModels.CustomerModel;
import com.example.jaderedworth.androidmobileapplication.R;// TODO: 04/05/2016

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

public class LoginFragment extends Fragment {

    private EditText emailInput;
    private String email;
    private EditText passwordInput;
    private String password;
    public Button loginButton;
    private int responseCode;
    private int userID;
    private CustomerModel customer;

    public LoginFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater i, ViewGroup vg, Bundle savedInstanceState) {
        super.onCreateView(i, vg, savedInstanceState);
        View rootView = i.inflate(R.layout.fragment_login, vg, false);

        emailInput = (EditText) rootView.findViewById(R.id.editTextLoginUsername);
        passwordInput = (EditText) rootView.findViewById(R.id.editTextLoginPassword);
        loginButton = (Button) rootView.findViewById(R.id.buttonLogin);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emailInput.getText().toString();
                password = passwordInput.getText().toString();

                if (!email.matches("") && !password.matches("")) {
                    PerformAuthentication(v);
                } else if (email.matches("")) {
                    Toast.makeText(getActivity(), "No Email inputted", Toast.LENGTH_LONG).show();

                } else if (password.matches("")) {
                    Toast.makeText(getActivity(), "No Password inputted", Toast.LENGTH_LONG).show();
                }
            }
        });
        return rootView;
    }

    public void PerformAuthentication(View v) {
        JSONObject JSONOutput = new JSONObject();
        try {
            JSONOutput.put("email", email);
            JSONOutput.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (JSONOutput.length() > 0) {
            new AsyncLoginTask(new AsyncLoginTask.AsyncResponse() {
                @Override
                public void processFinish(CustomerModel output) {
                    responseCode = output.getLogInResponseCode();
                    userID = output.getID();
                    if (responseCode == 200) {
                        customer = output;
                        Toast.makeText(getContext(), "Logged in as " + email, Toast.LENGTH_LONG).show();
                        AuthenticationHolder.setInstance(customer);
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        intent.putExtra("USERNAME", email);
                        intent.putExtra("PASSWORD", password);
                        intent.putExtra("USER_ID", userID);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), "Login failed.", Toast.LENGTH_LONG).show();
                    }
                }
            }).execute(JSONOutput.toString());
        }
    }
}
