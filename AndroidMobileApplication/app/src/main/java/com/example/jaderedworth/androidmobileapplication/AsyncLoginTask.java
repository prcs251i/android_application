package com.example.jaderedworth.androidmobileapplication;

import android.os.AsyncTask;
import android.util.Log;

import com.example.jaderedworth.androidmobileapplication.Fragments.LoginFragment;
import com.example.jaderedworth.androidmobileapplication.JSONModels.Address;
import com.example.jaderedworth.androidmobileapplication.JSONModels.CustomerModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by jaderedworth on 05/05/2016.
 */
public class AsyncLoginTask extends AsyncTask<String, String, CustomerModel> {

    private HttpURLConnection connection = null;
    private InputStream inputStream;
    private String JSONData;

    public interface AsyncResponse {
        void processFinish(CustomerModel output) throws IOException, JSONException;
    }

    public AsyncResponse delegate = null;

    public AsyncLoginTask(AsyncResponse delegate) {
        this.delegate = delegate;

    }

    @Override
    protected CustomerModel doInBackground(String... params) {

        CustomerModel  customerModel = new CustomerModel();
        JSONData = params[0];
        String serverResponse= null;
        BufferedReader r = null;

        try {
            URL targetURL = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/auth/customerAuthRequest");
            connection = (HttpURLConnection) targetURL.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            Writer w = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
            w.write((JSONData));
            w.close();

            int responseCode = connection.getResponseCode();

            inputStream = connection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            r = new BufferedReader(new InputStreamReader(inputStream));

            String lineFromInput;
            while ((lineFromInput = r.readLine())!=null){
                buffer.append(lineFromInput);
            }
            if (buffer.length() ==0){
                return null;
            }
            serverResponse = buffer.toString();
            JSONObject userObject = new JSONObject(serverResponse);
            customerModel.setID(userObject.getInt("ID"));
            customerModel.setEmail(userObject.getString("EMAIL"));
            customerModel.setSurname(userObject.getString("SURNAME"));
            customerModel.setForename(userObject.getString("FORENAME"));
            customerModel.setHomePhone(userObject.getString("HOME_PHONE"));
            customerModel.setMobPhone(userObject.getString("MOB_PHONE"));
            customerModel.setCardType(userObject.getString("CARD_TYPE"));
            customerModel.setCardNumber(userObject.getString("CARD_NUMBER"));
            String dateString = userObject.getString("EXPIRY_DATE");
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = formatter.parse(dateString);
            customerModel.setExpiryDate(date);
            customerModel.setSecurityNumber(userObject.getString("SECURITY_CODE"));
            Address address = new Address();
            address.setAddressLine1(userObject.getString("ADDRESS_LINE_1"));
            address.setAddressLine2(userObject.getString("ADDRESS_LINE_2"));
            address.setCity(userObject.getString("CITY"));
            address.setCounty(userObject.getString("COUNTY"));
            address.setPostcode(userObject.getString("POSTCODE"));
            customerModel.setAddress(address);
            customerModel.setLogInResponseCode(responseCode);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (r != null) {
                try {
                    r.close();
                } catch (final IOException e) {
                }
            }
        }
        return customerModel;
    }

    @Override
    protected void onPostExecute(CustomerModel result) {
        try {
            delegate.processFinish(result);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}