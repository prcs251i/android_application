package com.example.jaderedworth.androidmobileapplication.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jaderedworth.androidmobileapplication.AsyncLoginTask;
import com.example.jaderedworth.androidmobileapplication.BaseActivity.BaseUIActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TierModel;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R; // TODO: 04/05/2016

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PurchaseTicketsActivity extends BaseUIActivity implements Serializable {

    private String email;
    private Boolean loggedInCheck = false;
    private int tierID;
    private int quantity;
    private Double price;
    private String pr;
    private Double cost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchase_tickets);
        email = getIntent().getStringExtra("EMAIL");

        fillData();
    }

    private void fillData() {
        final TextView txtTicketType = (TextView) findViewById(R.id.textViewPurchaseTicketType);
        final TextView txtTicketTier = (TextView) findViewById(R.id.textViewPurchaseTicketTier);
        final TextView txtTicketQuantity = (TextView) findViewById(R.id.textViewPurchaseTicketQuantity);
        final TextView txtTicketPrice = (TextView) findViewById(R.id.textViewPurchaseTicketPrice);
        final TextView txtTicketTotalCost = (TextView) findViewById(R.id.textViewPurchaseTicketTotal);
        final Spinner spinnerTicketType = (Spinner) findViewById(R.id.spinnerTicketType);
        final Spinner spinnerTicketTier = (Spinner) findViewById(R.id.spinnerTicketTier);
        final Spinner spinnerQuantity = (Spinner) findViewById(R.id.spinnerQuantity);
        final Button btnContinue = (Button) findViewById(R.id.buttonContinue);
        txtTicketQuantity.setText("0");
        txtTicketPrice.setText("0");
        txtTicketTotalCost.setText("0");
        final HashMap<TierModel, Double> tierPriceList = (HashMap<TierModel, Double>) getIntent().getSerializableExtra("PRICE_LIST");
        final EventModel event = (EventModel) getIntent().getSerializableExtra("EVENT");
        final RunModel run = (RunModel) getIntent().getSerializableExtra("RUN");
        final int userID = getIntent().getIntExtra("USER_ID", 0);
        final int runID = run.getId();
        List<String> typeArray = new ArrayList();
        spinnerTicketType.setPrompt("Please Select a Ticket Type");
        typeArray.add("Seated");
        typeArray.add("Standing");
        setDataInSpinner(spinnerTicketType, (ArrayList) typeArray);
        List<Integer> quantityArray = new ArrayList();
        for (int i = 0; i < 10; i++) {
            quantityArray.add(i);
        }

        setDataInSpinner(spinnerQuantity, (ArrayList) quantityArray);
        assert spinnerTicketType != null;
        spinnerTicketType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String selectedItem = parent.getItemAtPosition(position).toString();
                txtTicketType.setText(selectedItem.toString());
                List<String> spinnerTierArray = new ArrayList();
                for (Map.Entry<TierModel, Double> e : tierPriceList.entrySet()) {
                    TierModel tier = e.getKey();
                    Double price = e.getValue();
                    if (selectedItem.equals("Seated")) {
                        if (price != null) {
                            if (tier.getIsSeated() == true) {
                                spinnerTierArray.add(tier.getTierId() + ": " + tier.getTierName() + " -  £" + price.toString() + "0");
                            }
                        }
                    } else if (selectedItem.equals("Standing")) {
                        if (price != null){
                        if (tier.getIsSeated() == false) {
                            spinnerTierArray.add(tier.getTierId() + ": " + tier.getTierName() + " -  £" + price.toString() + "0");
                        }
                    }
                }
                    assert spinnerTicketTier != null;
                }
                if (spinnerTierArray.isEmpty()) {
                    if (!spinnerTierArray.contains("Sorry no Tickets Available!")) {
                        spinnerTierArray.add("Sorry no Tickets Available!");
                    }
                }
                setDataInSpinner(spinnerTicketTier, (ArrayList) spinnerTierArray);
                spinnerTicketTier.setPrompt("Select an Option:");
                assert spinnerTicketTier != null;
                spinnerTicketTier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String selectedItem = parent.getItemAtPosition(position).toString();
                        String tierIDString = selectedItem.split(":", 2)[0];
                        try {
                            tierID = Integer.parseInt(tierIDString);
                            if (!selectedItem.toString().equals("Sorry no Tickets Available!")) {
                                txtTicketTier.setText(" in Tier: " + selectedItem);
                                String price = selectedItem.substring(selectedItem.length() - 6);
                                txtTicketPrice.setText(price);
                            }
                        } catch (NumberFormatException ex){
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        assert spinnerQuantity != null;
        spinnerQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                txtTicketQuantity.setText(selectedItem);
                    pr = txtTicketPrice.getText().toString().replace("£", "");
                    price = Double.valueOf(pr);
                    quantity = Integer.parseInt(txtTicketQuantity.getText().toString());
                    cost = price * quantity;
                    txtTicketTotalCost.setText("Total Cost: £" + String.valueOf(cost) + "0");
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        assert btnContinue != null;
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((Integer) spinnerQuantity.getSelectedItem()) != 0) {
                    Intent intent = new Intent(PurchaseTicketsActivity.this, ConfirmationActivity.class);
                    intent.putExtra("EVENT", event);
                    intent.putExtra("RUN", run);
                    intent.putExtra("VENUE", run.getVenue());
                    intent.putExtra("QUANTITY", txtTicketQuantity.getText().toString());
                    intent.putExtra("PRICE", txtTicketPrice.getText().toString());
                    intent.putExtra("TOTAL_COST", txtTicketTotalCost.getText().toString());
                    intent.putExtra("TICKET_TYPE", spinnerTicketType.getSelectedItem().toString());
                    intent.putExtra("TICKET_TIER", spinnerTicketTier.getSelectedItem().toString());
                    intent.putExtra("TIER_ID", tierID);
                    intent.putExtra("USER_ID", userID);
                    intent.putExtra("EMAIL", email);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please: Select a Quantity", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setDataInSpinner(Spinner id, ArrayList spinnerArray) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        id.setAdapter(adapter);
    }
}