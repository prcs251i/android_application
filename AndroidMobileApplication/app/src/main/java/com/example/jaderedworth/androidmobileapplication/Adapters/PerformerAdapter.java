package com.example.jaderedworth.androidmobileapplication.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.BaseActivity.BaseUIActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.PerformerModel;
import com.example.jaderedworth.androidmobileapplication.R; // TODO: 04/05/2016

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jaderedworth on 28/04/2016.
 */
public class PerformerAdapter extends ArrayAdapter {


    private ArrayList<PerformerModel> performerModelList;
    private int resource;
    private Context context;
    LayoutInflater inflater;
    private Bundle persistedData;

    public PerformerAdapter(Context context, int resource, List<PerformerModel> objects, Bundle bundle) {
        super(context, resource, objects);
        performerModelList = (ArrayList<PerformerModel>) objects;
        this.resource = resource;
        this.context = context;
        inflater = (LayoutInflater) getContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.persistedData = bundle;
    }

    @Override
    public int getCount() {
        return performerModelList.size();
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_performer_layout, null);

            viewHolder.performerName = (TextView) convertView.findViewById(R.id.textViewPerformerName);
            viewHolder.performerGenre = (TextView) convertView.findViewById(R.id.textViewPerformersGenre);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.performerName.setText(performerModelList.get(position).getPerformerName());
        viewHolder.performerGenre.setText(performerModelList.get(position).getActType());

        return convertView;
    }

    public class ViewHolder {
        TextView performerName;
        TextView performerGenre;
        Button view;
    }
}
