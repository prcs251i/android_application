package com.example.jaderedworth.androidmobileapplication.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jaderedworth.androidmobileapplication.AsyncLoginTask;
import com.example.jaderedworth.androidmobileapplication.BaseActivity.BaseUIActivity;
import com.example.jaderedworth.androidmobileapplication.Fragments.BookingFragment;
import com.example.jaderedworth.androidmobileapplication.Fragments.EventsFragment;
import com.example.jaderedworth.androidmobileapplication.JSONModels.AuthenticationHolder;
import com.example.jaderedworth.androidmobileapplication.JSONModels.Booking;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.JSONPostHandler;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R; // TODO: 04/05/2016

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

public class ConfirmationActivity extends BaseUIActivity implements Serializable {

    private int userID;
    private int tierID;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmation_page);

        userID = getIntent().getIntExtra("USER_ID", 0);
        email = AuthenticationHolder.getInstance().getEmail();

        fillData();
    }

    private void fillData() {
        TextView custEmail = (TextView) findViewById(R.id.textViewConfirmationEmail);
        TextView eventName = (TextView) findViewById(R.id.textViewConfirmationEvent);
        TextView venue = (TextView) findViewById(R.id.textViewConfirmationVenue);
        TextView venueAD1 = (TextView) findViewById(R.id.textViewConfirmationVenueAD1);
        TextView venueAD2 = (TextView) findViewById(R.id.textViewConfirmationVenueAD2);
        TextView venueCity = (TextView) findViewById(R.id.textViewConfirmationVenueCity);
        TextView venueCounty = (TextView) findViewById(R.id.textViewConfirmationVenueCounty);
        TextView venuePostcode = (TextView) findViewById(R.id.textViewConfirmationVenuePostcode);
        TextView runDate = (TextView) findViewById(R.id.textViewConfirmationDate);
        TextView runTime = (TextView) findViewById(R.id.textViewConfirmationTime);

        TextView ticketType = (TextView) findViewById(R.id.textViewConfirmationTickType);
        TextView ticketTier = (TextView) findViewById(R.id.textViewConfirmationTickTier);
        final TextView ticketQuantity = (TextView) findViewById(R.id.textViewConfirmationTickQuantity);
        TextView ticketPrice = (TextView) findViewById(R.id.textViewConfirmationTickPrice);
        final TextView totalCost = (TextView) findViewById(R.id.textViewConfirmationTickTotalCost);
        Button btnConfirm = (Button) findViewById(R.id.buttonConfirm);

        EventModel event = (EventModel) getIntent().getSerializableExtra("EVENT");
        final RunModel run = (RunModel) getIntent().getSerializableExtra("RUN");
        userID = getIntent().getIntExtra("USER_ID", 0);
        tierID = getIntent().getIntExtra("TIER_ID", 0);
        eventName.setText(event.getTitle());
        venue.setText(run.getVenue().getVenueName());
        venueAD1.setText(run.getVenue().getVenueAddressLine1());
        venueAD2.setText(run.getVenue().getVenueAddressLine2());
        venueCity.setText(run.getVenue().getVenueAddressCity());
        venueCounty.setText(run.getVenue().getVenueAddressCounty());
        venuePostcode.setText(run.getVenue().getVenueAddressPostcode());
        String date = run.getDate().split("T", 2)[0];
        String time = run.getDate().split("T", 2)[1];
        runDate.setText(date);
        runTime.setText(time);
        ticketType.setText(getIntent().getStringExtra("TICKET_TYPE"));
        String ticketTierString = getIntent().getStringExtra("TICKET_TIER");
        ticketTier.setText(ticketTierString.split("-", 2)[0]);
        ticketQuantity.setText(getIntent().getStringExtra("QUANTITY"));
        ticketPrice.setText(getIntent().getStringExtra("PRICE"));
        totalCost.setText(getIntent().getStringExtra("TOTAL_COST").replace("Total Cost: ", ""));
        custEmail.setText(email);
        assert btnConfirm != null;
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date today = new Date();
                double totalCostSubstring = Double.parseDouble(totalCost.getText().toString().replace("£", ""));
                final Booking newBooking = new Booking(run.getId(), userID, today, Integer.parseInt(ticketQuantity.getText().toString()), totalCostSubstring);
                final JSONObject bookingObject = new JSONObject();
                try {
                    bookingObject.put("runID", newBooking.getRunID());
                    bookingObject.put("personID", newBooking.getCustomer());
                    bookingObject.put("tierID", tierID);
                    bookingObject.put("quantity", newBooking.getNoOfTickets());
                    bookingObject.put("totalCost", newBooking.getTotalCost());
                    final String data = bookingObject.toString();
                    final URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/bookings/createBooking");
                    new JSONPostHandler(new JSONPostHandler.AsyncPostResponse() {
                        @Override
                        public void processFinish(String output) {
                            AlertDialog.Builder builder;
                            if (output.contains("20")) {
                                builder = new AlertDialog.Builder(ConfirmationActivity.this);
                                AlertDialog dialog = builder.create();
                                builder.setTitle("Success");
                                builder.setMessage("Your Booking was successful");
                                builder.setPositiveButton("Home", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ConfirmationActivity.this.startActivity(new Intent(ConfirmationActivity.this, MainActivity.class).putExtra("USER_ID", userID));
                                    }
                                });
                                builder.create().show();
                            } else if (output.equals("401Unauthorized")) {
                                builder = new AlertDialog.Builder(ConfirmationActivity.this);
                                builder.setMessage("You are not logged in!").setTitle("Warning!");
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            } else {
                                builder = new AlertDialog.Builder(ConfirmationActivity.this);
                                builder.setMessage("Sorry, we were unable to book you tickets a this time. Please try again later").setTitle("Warning!");
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    }).execute(data, url.toString());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}