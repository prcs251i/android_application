package com.example.jaderedworth.androidmobileapplication.JSONModels;

public abstract class Person {

    private int id;
    private String eMail;
    private String password;

    public Person() {

    }

    public Person(int personId, String eMailAdd, String pass) {
        id = personId;
        eMail = eMailAdd;
        password = pass;
    }

    public int getID() {
        return this.id;
    }

    public void setID(int newId) {
        this.id = newId;
    }

    public String getEmail() {
        return this.eMail;
    }

    public void setEmail(String newEmail) {
        this.eMail = newEmail;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String newPass) {
        this.password = newPass;
    }

}
