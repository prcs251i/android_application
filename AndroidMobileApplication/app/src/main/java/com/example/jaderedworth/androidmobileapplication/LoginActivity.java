package com.example.jaderedworth.androidmobileapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.jaderedworth.androidmobileapplication.Fragments.RegisterFragmentMain;

public class LoginActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.setTitle("Log In");
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        toolbar = (Toolbar)findViewById(R.id.toolbarLogin);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.login_menu, m);
        return true;

    }

    public void registerButtonOnClick(View v){
        RegisterFragmentMain targetFragment = new RegisterFragmentMain(getApplicationContext());
        getFragmentManager().beginTransaction().replace(R.id.fragment_login_main, targetFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem m){
        this.finish();
        return true;
    }



}
