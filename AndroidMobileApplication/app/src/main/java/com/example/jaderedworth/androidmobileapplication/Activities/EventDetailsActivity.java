package com.example.jaderedworth.androidmobileapplication.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.Adapters.PerformerAdapter;
import com.example.jaderedworth.androidmobileapplication.AsyncLoginTask;
import com.example.jaderedworth.androidmobileapplication.BaseActivity.BaseUIActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.PerformerModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TierModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.VenueModel;
import com.example.jaderedworth.androidmobileapplication.LoginActivity;
import com.example.jaderedworth.androidmobileapplication.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;

public class EventDetailsActivity extends BaseUIActivity implements Serializable {

    private int userID;
    private String email;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        bundle = getIntent().getExtras();
        userID = getIntent().getIntExtra("USER_ID", 0);
        email = getIntent().getStringExtra("EMAIL");
        fillDetails();
    }

    private void fillDetails() {
        ImageView imgPic = (ImageView) findViewById(R.id.imageView);
        final TextView txtEventName = (TextView) findViewById(R.id.textViewEventDetailsEventName);
        final TextView txtEventDate = (TextView) findViewById(R.id.textViewEventDetailsDate);
        final TextView txtDuration = (TextView) findViewById(R.id.textViewEventDuration);
        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setEnabled(false);
        TextView txtEventDesc = (TextView) findViewById(R.id.textViewEventDetailsDescription);
        Button btnBuyTickets = (Button) findViewById(R.id.buttonBuyTickets);
        ListView lstPerformers = (ListView) findViewById(R.id.listViewPerformers);

        final HashMap<TierModel, Double> tierPriceList = (HashMap<TierModel, Double>) getIntent().getSerializableExtra("PRICE_LIST");
        ArrayList<PerformerModel> performerList = (ArrayList<PerformerModel>) getIntent().getSerializableExtra("PERFORMER_LIST");
        final VenueModel venue = (VenueModel) getIntent().getSerializableExtra("VENUE");
        final EventModel event = (EventModel) getIntent().getSerializableExtra("EVENT");
        final RunModel run = (RunModel) getIntent().getSerializableExtra("RUN");

        String date = run.getDate().split("T", 2)[0];
        String time = run.getDate().split("T", 2)[1];
        String duration = String.valueOf(run.getDuration());

        Picasso.with(getApplicationContext()).load(event.getImage()).into(imgPic);
        txtEventName.setText(event.getTitle() + " @ " + venue.getVenueName());
        txtEventDate.setText(date + " @ " + time);
        txtDuration.setText(duration + " mins");

        txtEventDesc.setText(event.getDescription());
        ratingBar.setRating((float) event.getRating());
        if (performerList != null) {
            PerformerAdapter adapter = new PerformerAdapter(getApplicationContext(), R.layout.list_performer_layout, performerList, bundle);
            if (lstPerformers != null) {
                lstPerformers.setAdapter(adapter);
            }
        }

        btnBuyTickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EventDetailsActivity.this, PurchaseTicketsActivity.class);
                intent.putExtra("PRICE_LIST", tierPriceList);
                intent.putExtra("EVENT", event);
                intent.putExtra("RUN", run);
                intent.putExtra("USER_ID", userID);
                intent.putExtra("EMAIL", email);
                startActivity(intent);
            }
        });
    }
}