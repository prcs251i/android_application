package com.example.jaderedworth.androidmobileapplication.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.Activities.EventDetailsActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.PerformerModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TierModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.VenueModel;
import com.example.jaderedworth.androidmobileapplication.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jaderedworth on 26/04/2016.
 */
public class RunsAdapter extends ArrayAdapter {

    private ArrayList<RunModel> runModelList;
    private EventModel eventModel;
    private ListView list;
    private int userID;
    private int resource;
    private String email;
    private Context context;
    LayoutInflater inflater;
    private Bundle persistedData;

    public RunsAdapter(Context context, int resource, List<RunModel> objects, EventModel event, String email, int userID, Bundle bundle) {
        super(context, resource, objects);
        runModelList = (ArrayList<RunModel>) objects;
        this.resource = resource;
        this.context = context;
        this.eventModel = event;
        this.userID = userID;
        this.email = email;
        this.persistedData = bundle;
        inflater = (LayoutInflater) getContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return runModelList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_runs_layout, null);

            viewHolder.runVenue = (TextView) convertView.findViewById(R.id.textViewRunsVenue);
            viewHolder.runDate = (TextView) convertView.findViewById(R.id.textViewRunsDate);
            viewHolder.runTime = (TextView) convertView.findViewById(R.id.textViewRunsTime);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.imageViewRuns);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final VenueModel venue = runModelList.get(position).getVenue();

        viewHolder.runVenue.setText(venue.getVenueName());
        final String date = runModelList.get(position).getDate().split("T", 2)[0];
        final String time = runModelList.get(position).getDate().split("T", 2)[1];
        viewHolder.runDate.setText("Date: " + date);
        viewHolder.runTime.setText("Start Time: " + time);
        Picasso.with(context).load(eventModel.getImage()).into(viewHolder.image);
        //viewHolder.eventRating.setRating(eventModelList.get(position).getRating());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final HashMap<TierModel, Double> priceList = runModelList.get(position).getTierPrices();
                final List<PerformerModel> performerList = runModelList.get(position).getPerformers();
                Intent intent = new Intent(context, EventDetailsActivity.class);

                intent.putExtras(persistedData);
                intent.putExtra("PRICE_LIST", (Serializable) priceList);
                intent.putExtra("PERFORMER_LIST", (Serializable) performerList);
                intent.putExtra("EVENT", (Serializable) eventModel);
                intent.putExtra("RUN", runModelList.get(position));
                intent.putExtra("VENUE", venue);
                intent.putExtra("USER_ID", userID);
                intent.putExtra("EMAIL", email);
                context.startActivity(intent);
                Log.d("CLICK", "onClick: OPEN Activity");
            }
        });


        return convertView;
    }

    public class ViewHolder {
        TextView runVenue;
        TextView runDate;
        TextView runTime;
        ImageView image;
    }
}

