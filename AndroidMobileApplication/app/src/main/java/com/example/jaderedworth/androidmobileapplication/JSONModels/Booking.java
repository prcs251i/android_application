package com.example.jaderedworth.androidmobileapplication.JSONModels;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Booking implements Serializable {

    private int bookingID;
    private int runID;
    private int customerID;
    private Date datePlaced;
    private int noOfTickets;
    private List<TicketModel> orderedTicketModels;
    private double totalCost;
    private EventTypeModel eventTypeModel;

    public Booking() {
        
    }
    
    public Booking(int newRunID, int newCustID, Date bookingDate, int quantity, double bookingTotalCost) {
        runID = newRunID;
        customerID = newCustID;
        datePlaced = bookingDate;
        orderedTicketModels = new ArrayList();
        noOfTickets = quantity;
        totalCost = bookingTotalCost;
    }

    public int getBookingID() {
        return this.bookingID;
    }
    
    public void setBookingID(int newBookingID) {
        this.bookingID = newBookingID;
    }

    public int getCustomer() {
        return this.customerID;
    }
    
    public void setCustomer(int newCustomer) {
        this.customerID = newCustomer;
    }

    public int getRunID() {
        return this.runID;
    }
    
    public void setRunID(int newRun) {
        this.runID = newRun;
    }

    public Date getDatePlaced() {
        return this.datePlaced;
    }
    
    public void setDatePlaced(Date newDatePlaced) {
        this.datePlaced = newDatePlaced;
    }
    
    public void setPersonID(Date newDatePlaced) {
        this.datePlaced = newDatePlaced;
    }
    
    public int getNoOfTickets() {
        return this.noOfTickets;
    }
    
    public void setNoOfTickets(int newNoOfTickets) {
        this.noOfTickets = newNoOfTickets;
    }

    public List<TicketModel> getTicketList() {
        return this.orderedTicketModels;
    }
    
    public void setTicketList(List<TicketModel> ticketModelList) {
        this.orderedTicketModels = ticketModelList;
    }

    public double getTotalCost() {
        return this.totalCost;
    }
    
    public void setTotalCost(double newTotalCost) {
        this.totalCost = newTotalCost;
    }

    public EventTypeModel getEventTypeModel() {
        return eventTypeModel;
    }

    public void setEventTypeModel(EventTypeModel eventTypeModel) {
        this.eventTypeModel = eventTypeModel;
    }

}
