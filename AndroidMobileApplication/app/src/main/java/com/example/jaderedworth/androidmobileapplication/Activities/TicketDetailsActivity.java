package com.example.jaderedworth.androidmobileapplication.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jaderedworth.androidmobileapplication.Adapters.TicketAdapter;
import com.example.jaderedworth.androidmobileapplication.BaseActivity.BaseUIActivity;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.RunModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.TicketModel;
import com.example.jaderedworth.androidmobileapplication.R;

import java.io.Serializable;
import java.util.List;

public class TicketDetailsActivity extends BaseUIActivity implements Serializable{

    private TicketAdapter adapter;
    private EventModel event;
    private RunModel run;
    private List<TicketModel> tickets;
    private TextView txtBookingRef;
    private TextView txtEventName;
    private TextView txtVenueName;
    private TextView txtDate;
    private TextView txtTime;
    private TextView txtDuration;
    private Bundle bundle;
    private ListView ticketList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_details);

        bundle = getIntent().getExtras();
        event = (EventModel) getIntent().getSerializableExtra("EVENT");
        run = (RunModel) getIntent().getSerializableExtra("RUN");
        tickets = (List<TicketModel>) getIntent().getSerializableExtra("TICKETS");
        txtBookingRef = (TextView) findViewById(R.id.textViewEventBookingRef);
        txtEventName = (TextView) findViewById(R.id.textViewTicketEventName);
        txtVenueName = (TextView) findViewById(R.id.textViewTicketVenueName);
        txtDate = (TextView) findViewById(R.id.textViewTicketDate);
        txtTime = (TextView) findViewById(R.id.textViewTicketTime);
        txtDuration = (TextView) findViewById(R.id.textViewTicketDuration);
        ticketList = (ListView) findViewById(R.id.listViewTickets);

        fillData();

    }

    private void fillData() {
        txtBookingRef.setText("Booking Reference Number: " + getIntent().getStringExtra("BOOKING_REF"));
        txtEventName.setText("Event: " + getIntent().getStringExtra("EVENT_NAME"));
        txtVenueName.setText("Venue: " + getIntent().getStringExtra("VENUE_NAME"));
        String date = run.getDate().split("T",2)[0];
        String time = run.getDate().split("T",2)[1];
        txtDate.setText("Date: " + date);
        txtTime.setText("Start Time: " + time);
        txtDuration.setText("Duration: " + run.getDuration() + "mins");
        fillAdapter(tickets);
    }

    public void fillAdapter(List<TicketModel> data) {
        adapter = new TicketAdapter(getApplicationContext(), R.layout.list_ticket_layout, data, bundle);
        if (ticketList != null) {
            if (data != null) {
                ticketList.setAdapter(adapter);
            }
        }
    }
}