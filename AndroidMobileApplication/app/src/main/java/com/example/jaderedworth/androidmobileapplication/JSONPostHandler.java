package com.example.jaderedworth.androidmobileapplication;

import android.os.AsyncTask;
import android.util.Log;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by jaderedworth on 06/05/2016.
 */
public class JSONPostHandler extends AsyncTask<String, String, String> {

    public interface AsyncPostResponse {
        void processFinish(String output);
    }

    public AsyncPostResponse delegate = null;

    public JSONPostHandler(AsyncPostResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(String... params) {
        String data = params[0];
        URL url = null;
        try {
            url = new URL(params[1]);
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
        HttpURLConnection conn = null;
        String info = null;
        try {
            byte[] postData = data.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.write(postData);
            wr.close();

            try {
                info = conn.getResponseCode() + conn.getResponseMessage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException ex) {
        } catch (ProtocolException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        conn.disconnect();
        return info;
    }

        @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }
}
