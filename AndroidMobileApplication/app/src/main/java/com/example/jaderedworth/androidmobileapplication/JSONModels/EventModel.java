package com.example.jaderedworth.androidmobileapplication.JSONModels;

import android.media.Image;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jaderedworth on 22/03/2016.
 */
public class EventModel implements Serializable {

    private int id;
    private String eventType;
    private String title;
    private String age;
    private double rating;
    private String description;
    private String image;
    private List<RunModel> runs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<RunModel> getRuns() {
        return runs;
    }

    public void setRuns(List<RunModel> runs) {
        this.runs = runs;
    }

}