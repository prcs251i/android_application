package com.example.jaderedworth.androidmobileapplication.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jaderedworth.androidmobileapplication.Activities.MainActivity;
import com.example.jaderedworth.androidmobileapplication.AsyncLoginTask;
import com.example.jaderedworth.androidmobileapplication.JSONModels.AuthenticationHolder;
import com.example.jaderedworth.androidmobileapplication.JSONModels.Booking;
import com.example.jaderedworth.androidmobileapplication.JSONModels.CustomerModel;
import com.example.jaderedworth.androidmobileapplication.JSONModels.EventModel;
import com.example.jaderedworth.androidmobileapplication.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class UserAccountFragment extends Fragment implements Serializable{

    private Bundle bundle;
    private Context c;
    private CustomerModel customer;
    private String email;
    private TextView homePhone;
    private TextView mobPhone;
    private TextView cardType;
    private TextView cardNo;
    private TextView expiryDate;
    private TextView addressLine1;
    private TextView addressLine2;
    private TextView city;
    private TextView county;
    private TextView postcode;
    private int userID;

    public UserAccountFragment() {
    }

    public UserAccountFragment(Context context) {
        c = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_user_account, container, false);

        bundle = this.getArguments();
        if (bundle != null) {
            customer = AuthenticationHolder.getInstance();
            userID = bundle.getInt("USER_ID", 0);
        }
        homePhone = (TextView) rootView.findViewById(R.id.textViewHomePhone);
        mobPhone = (TextView) rootView.findViewById(R.id.textViewMobPhone);
        cardType = (TextView) rootView.findViewById(R.id.textViewCardType);
        cardNo = (TextView) rootView.findViewById(R.id.textViewCardNumber);
        expiryDate = (TextView) rootView.findViewById(R.id.textViewExpiryDate);
        addressLine1 = (TextView) rootView.findViewById(R.id.textViewAddressLine1);
        addressLine2 = (TextView) rootView.findViewById(R.id.textViewAddressLine2);
        city = (TextView) rootView.findViewById(R.id.textViewCity);
        county = (TextView) rootView.findViewById(R.id.textViewCounty);
        postcode = (TextView) rootView.findViewById(R.id.textViewPostcode);

        if (userID!=0) {
            fillData();
        }
        else {

        }
        return rootView;
    }

    private void fillData() {
        try {
            homePhone.append(customer.getHomePhone());
            mobPhone.append(customer.getMobPhone());
            cardType.append(customer.getCardType());
            cardNo.append(customer.getCardNumber());
            addressLine1.append(customer.getAddress().getAddressLine1());
            addressLine2.append(customer.getAddress().getAddressLine2());
            city.append(customer.getAddress().getCity());
            county.append(customer.getAddress().getCounty());
            postcode.append(customer.getAddress().getPostcode());
            String dateString = new SimpleDateFormat("dd-MM-yyyy").format(customer.getExpiryDate());
            expiryDate.append(dateString);
        } catch (NullPointerException ex){

        }
    }
}